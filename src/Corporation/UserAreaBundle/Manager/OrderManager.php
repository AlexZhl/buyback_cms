<?php

namespace Corporation\UserAreaBundle\Manager;

use Sonata\UserBundle\Model\UserInterface;

class OrderManager extends BaseManager
{
    public function findForUser(UserInterface $user, array $orderBy = [], $limit = null, $offset = null)
    {
        return [];
        $qb = $this->getRepository()->createQueryBuilder('o')
            ->leftJoin('o.customer', 'c')
            ->where('c.user = :user')
            ->setParameter('user', $user);
        foreach ($orderBy as $field => $dir) {
            $qb->orderBy('o.'.$field, $dir);
        }
        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }
        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }
        return $qb->getQuery()->execute();
    }
}
