<?php

namespace Corporation\UserAreaBundle\Manager;

use Knp\Component\Pager\Paginator;

class ReviewManager extends BaseManager
{
    public function getItemsWithPagination($page, $itemsPerPage)
    {
        $qb = $this->getRepository()->createQueryBuilder('review');
        $query = $qb
            ->andWhere('review.enabled = :yes')
            ->setParameters(['yes' => 1])
            ->getQuery();

        return $this->getPaginator()->paginate($query, $page, $itemsPerPage);
    }
}
