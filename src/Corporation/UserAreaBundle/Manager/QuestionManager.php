<?php

namespace Corporation\UserAreaBundle\Manager;

use Knp\Component\Pager\Paginator;

class QuestionManager extends BaseManager
{
    public function getItemsWithPagination($page, $itemsPerPage)
    {
        $qb = $this->getRepository()->createQueryBuilder('question');
        $query = $qb
            ->andWhere('question.enabled = :yes')
            ->setParameters(['yes' => 1])
            ->getQuery();

        return $this->getPaginator()->paginate($query, $page, $itemsPerPage);
    }
}
