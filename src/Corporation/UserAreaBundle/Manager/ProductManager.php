<?php

namespace Corporation\UserAreaBundle\Manager;

use Knp\Component\Pager\Paginator;

class ProductManager extends BaseManager
{
    public function getProductsWithPagination($page, $itemsPerPage, $category = null)
    {
        $qb = $this->repository
            ->createQueryBuilder('product')
//            ->from($this->class, 'p')
            ->leftJoin('product.categories', 'category')
            ->andWhere('product.enabled = :yes')
            ->setParameter('yes', 1)
        ;
        if ($category) {
            $qb->andWhere('category.id = :categoryId');
            $qb->setParameter('categoryId', $category);
        }
        $query = $qb->getQuery();

        return $this->getPaginator()->paginate($query, $page, $itemsPerPage);
    }

    public function searchByTitle($searchTerm)
    {
        return $this->repository->createQueryBuilder('p')
            ->where('p.title LIKE :searchTerm')
            ->setParameter('searchTerm', '%' . $searchTerm . '%')
            ->getQuery()
            ->getResult();
    }
}
