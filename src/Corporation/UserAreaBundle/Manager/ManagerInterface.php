<?php

namespace Corporation\UserAreaBundle\Manager;

use Doctrine\ORM\EntityManager;

interface ManagerInterface
{
    /**
     * @return EntityManager
     */
    public function getEm();

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository();

    /**
     * @return Knp\Component\Pager\Paginator
     */
    public function getPaginator();

    /**
     * @return mixed
     */
    public function newEntity();

    /**
     * @param $entity
     * @param bool $andFlush
     */
    public function saveEntity($entity, $andFlush = true);

    /**
     * @return mixed
     */
    public function flush();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param array $array
     *
     * @return object
     */
    public function findOneBy(array $array);

    /**
     * @return array
     */
    public function findAll();

    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @return string
     */
    public function getClass();

    /**
     * @param $entity
     */
    public function deleteEntity($entity);

    /**
     * @param array $entities
     * @param int   $batchSize
     */
    public function deleteEntities(array $entities, $batchSize);

    /**
     * @param array $criteria
     *
     * @return object
     */
    public function findEntityBy(array $criteria);

    /**
     * @param $entity
     */
    public function reloadEntity($entity);
}
