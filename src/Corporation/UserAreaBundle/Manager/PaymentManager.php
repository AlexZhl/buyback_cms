<?php

namespace Corporation\UserAreaBundle\Manager;

use Corporation\AdminAreaBundle\Entity\Customer;
use Corporation\AdminAreaBundle\Entity\Discount;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class PaymentManager extends BaseManager
{
    const RESPONSE_OK = 'Ok';

    private $authorizeNetApiLoginId;
    private $authorizeNetTransactionKey;
    private $responseData = [
        'success' => true,
        'message' => '',
        'messageCode' => '',
        'transactionId' => '',
    ];

    public function __construct($authorizeNetApiLoginId, $authorizeNetTransactionKey)
    {
        $this->authorizeNetApiLoginId = $authorizeNetApiLoginId;
        $this->authorizeNetTransactionKey = $authorizeNetTransactionKey;
    }

    public function processPayment(Customer $customer, $amount, Discount $discount = null)
    {
        /* Create a merchantAuthenticationType object with authentication details retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->authorizeNetApiLoginId);
        $merchantAuthentication->setTransactionKey($this->authorizeNetTransactionKey);

        // Set the transaction's refId
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($customer->getCardNumber());
        $creditCard->setExpirationDate($customer->getCardYear() . '-' . $customer->getCardMonth());
        $creditCard->setCardCode($customer->getCardCVV());

        // Add the payment data to a paymentType object
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

//        // Create order information
        $order = new AnetAPI\OrderType();
//        $order->setInvoiceNumber("10101");
        $order->setDescription('Seasonings');

        // Set the customer's Bill To address
        $customerBillingAddress = new AnetAPI\CustomerAddressType();
        $customerBillingAddress->setFirstName($customer->getBillingFirstname());
        $customerBillingAddress->setLastName($customer->getBillingLastname());
        $customerBillingAddress->setAddress($customer->getBillingAddressLine1());
        $customerBillingAddress->setCity($customer->getBillingCity());
        $customerBillingAddress->setState($customer->getBillingRegion());
        $customerBillingAddress->setZip($customer->getBillingPostalCode());
        $customerBillingAddress->setCountry($customer->getBillingCountry());

        // Set the customer's Ship To address
        $customerShippingAddress = new AnetAPI\CustomerAddressType();
        $customerShippingAddress->setFirstName($customer->getFirstname());
        $customerShippingAddress->setLastName($customer->getLastname());
        $customerShippingAddress->setAddress($customer->getAddressLine1());
        $customerShippingAddress->setCity($customer->getCity());
        $customerShippingAddress->setState($customer->getRegion());
        $customerShippingAddress->setZip($customer->getPostalCode());
        $customerShippingAddress->setCountry($customer->getCountry());

//        // Set the customer's identifying information
//        $customerData = new AnetAPI\CustomerDataType();
//        $customerData->setType("individual");
//        $customerData->setId("99999456654");
//        $customerData->setEmail("EllenJohnson@example.com");

        // Add values for transaction settings
        $duplicateWindowSetting = new AnetAPI\SettingType();
        $duplicateWindowSetting->setSettingName("duplicateWindow");
        $duplicateWindowSetting->setSettingValue("60");

//        // Add some merchant defined fields. These fields won't be stored with the transaction,
//        // but will be echoed back in the response.
//        $merchantDefinedField1 = new AnetAPI\UserFieldType();
//        $merchantDefinedField1->setName("customerLoyaltyNum");
//        $merchantDefinedField1->setValue("1128836273");
//
//        $merchantDefinedField2 = new AnetAPI\UserFieldType();
//        $merchantDefinedField2->setName("favoriteColor");
//        $merchantDefinedField2->setValue("blue");

        // Create a TransactionRequestType object and add the previous objects to it
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType('authCaptureTransaction');
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setShipTo($customerShippingAddress);
        $transactionRequestType->setBillTo($customerBillingAddress);
//        $transactionRequestType->setCustomer($customerData);
        $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
//        $transactionRequestType->addToUserFields($merchantDefinedField1);
//        $transactionRequestType->addToUserFields($merchantDefinedField2);

        // Assemble the complete transaction request
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        // Create the controller and get the response
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

        if ($response != null) {
            // Check to see if the API request was successfully received and acted upon
            if ($response->getMessages()->getResultCode() == self::RESPONSE_OK) {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $this->responseData['messageCode'] = $tresponse->getMessages()[0]->getCode();
                    $this->responseData['message'] = $tresponse->getMessages()[0]->getDescription();
                    $this->responseData['transactionId'] = $tresponse->getTransId();
                } else {
                    $this->responseData['success'] = false;
                    if ($tresponse->getErrors() != null) {
                        $this->responseData['messageCode'] = $tresponse->getErrors()[0]->getErrorCode();
                        $this->responseData['message'] = $tresponse->getErrors()[0]->getErrorText();
                    }
                }
                // Or, print errors if the API request wasn't successful
            } else {
                $this->responseData['success'] = false;
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $this->responseData['messageCode'] = $tresponse->getErrors()[0]->getErrorCode();
                    $this->responseData['message'] = $tresponse->getErrors()[0]->getErrorText();
                } else {
                    $this->responseData['messageCode'] = $response->getMessages()->getMessage()[0]->getCode();
                    $this->responseData['message'] = $response->getMessages()->getMessage()[0]->getText();
                }
            }
        } else {
            $this->responseData['success'] = false;
            $this->responseData['message'] = 'No response from payment processor';
        }

        return $this->responseData;
    }
}
