<?php

namespace Corporation\UserAreaBundle\Manager;

use Corporation\AdminAreaBundle\Entity\Customer;
use Corporation\AdminAreaBundle\Entity\Discount;
use Symfony\Component\Intl\Intl;
use USPS\RatePackage;

class UspsManager extends BaseManager
{
    private $apiUsername;
    private $customer;
    private $originZip;

    public function __construct($apiUsername, $originZip)
    {
        $this->apiUsername = $apiUsername;
        $this->originZip = $originZip;
    }

    public function calculateRates(Customer $customer)
    {
        $this->customer = $customer;
        if ($this->customer->getCountry() == 'US') {
            return $this->calculateDomestic();
        } else {
            return $this->calculateInternational();
        }
    }

    public function calculateDomestic()
    {
        $rate = new \USPS\Rate($this->apiUsername);

        // Create new package object and assign the properties
        // apartently the order you assign them is important so make sure
        // to set them as the example below
        // set the RatePackage for more info about the constants

        $package = new RatePackage();
        $package->setService(RatePackage::SERVICE_ALL);
        $package->setFirstClassMailType(RatePackage::MAIL_TYPE_FLAT);
        $package->setZipOrigination($this->originZip);
        $package->setZipDestination($this->customer->getPostalCode());
        $package->setPounds(0);
        $package->setOunces(3.5);
        $package->setContainer('');
        $package->setSize(RatePackage::SIZE_REGULAR);
        $package->setContainer(RatePackage::CONTAINER_VARIABLE);
        $package->setField('Machinable', true);

        // add the package to the rate stack
        $rate->addPackage($package);

        // Perform the request and print out the result
        $rate->getRate();

        // Was the call successful
        if ($rate->isSuccess()) {
            $ratesData = [
                'success' => true,
                'rates' => [],
            ];
            $calculatedRates = $rate->getArrayResponse()['RateV4Response']['Package']['Postage'];

            foreach ($calculatedRates as $calculatedRate) {
                $ratesData['rates'][] = [
                    'description' => $calculatedRate['MailService'],
                    'price' => $calculatedRate['Rate']
                ];
            }
        } else {
            $ratesData = [
                'success' => false,
                'message' => $rate->getErrorMessage(),
            ];
        }

        return $ratesData;
    }

    public function calculateInternational()
    {

        $rate = new \USPS\Rate($this->apiUsername);
        $rate->setInternationalCall(true);
        $rate->addExtraOption('Revision', 2);

        $package = new RatePackage;
        $package->setPounds(0);
        $package->setOunces(3.15);
        $package->setField('Machinable', 'True');
        $package->setField('MailType', 'SM FLAT RATE BOX');
//        $package->setField('GXG', array(
//            'POBoxFlag' => 'Y',
//            'GiftFlag' => 'Y'
//        ));
        $package->setField('ValueOfContents', 5);
        $package->setField('Country', Intl::getRegionBundle()->getCountryName($this->customer->getCountry()));
        $package->setField('Container', 'RECTANGULAR');
        $package->setField('Size', 'REGULAR');
        $package->setField('Width', 10);
        $package->setField('Length', 15);
        $package->setField('Height', 10);
        $package->setField('Girth', 0);
        $package->setField('OriginZip', $this->originZip);
        $package->setField('CommercialFlag', 'N');
        $package->setField('AcceptanceDateTime', date('c'));
        $package->setField('DestinationPostalCode', $this->customer->getPostalCode());

        $rate->addPackage($package);
        $rate->getRate();

        if ($rate->isSuccess()) {
            $ratesData = [
                'success' => true,
                'rates' => [],
            ];
            $calculatedRates = $rate->getArrayResponse()['IntlRateV2Response']['Package']['Service'];

            foreach ($calculatedRates as $calculatedRate) {
                $ratesData['rates'][] = [
                    'description' => $calculatedRate['SvcCommitments'],
                    'price' => $calculatedRate['Postage']
                ];
            }
        } else {
            $ratesData = [
                'success' => false,
                'message' => $rate->getErrorMessage(),
            ];
        }

        return $ratesData;
    }
}
