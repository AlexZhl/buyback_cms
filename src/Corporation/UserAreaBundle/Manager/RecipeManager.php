<?php

namespace Corporation\UserAreaBundle\Manager;

use Knp\Component\Pager\Paginator;

class RecipeManager extends BaseManager
{
    public function getItemsWithPagination($page, $itemsPerPage)
    {
        $qb = $this->getRepository()->createQueryBuilder('recipe');
        $query = $qb
            ->andWhere('recipe.enabled = :yes')
            ->setParameters(['yes' => 1])
            ->getQuery();

        return $this->getPaginator()->paginate($query, $page, $itemsPerPage);
    }
}
