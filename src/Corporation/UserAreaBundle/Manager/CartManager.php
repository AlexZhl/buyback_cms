<?php

namespace Corporation\UserAreaBundle\Manager;

use Corporation\AdminAreaBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\Session;

class CartManager
{
    private $session;
    private $emptyCart = [
        'products' => [],
        'amount' => 0,
    ];

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function getCart()
    {
        $cart = $this->session->get('cart', []);

        if (!$cart) {
            $cart = $this->emptyCart;
        }

        return $cart;
    }

    public function saveCart($cart)
    {
        $this->session->set('cart', $cart);
    }

    public function clearCart()
    {
        $this->session->set('cart', $this->emptyCart);
    }

    public function addProduct(Product $product, $quantity)
    {
        $cart = $this->getCart();

        $productId = $product->getId();

        if (array_key_exists($productId, $cart['products'])) {
            $cart['products'][$productId]['quantity'] = $quantity;
        } else {
            $cart['products'][$productId] = [
                'id' => $productId,
                'title' => $product->getTitle(),
                'mainPhoto' => $product->getMainPhoto()->getId(),
                'price' => $product->getPrice(),
                'quantity' => $quantity,
            ];
        }

        $cart['amount'] = $this->calculateAmount($cart);
        $this->saveCart($cart);

        return $cart;
    }

    public function removeProduct($id)
    {
        $cart = $this->getCart();

        if (array_key_exists($id, $cart['products'])) {
            unset($cart['products'][$id]);
        }

        $cart['amount'] = $this->calculateAmount($cart);
        $this->saveCart($cart);

        return $cart;
    }

    private function calculateAmount($cart)
    {
        $amount = 0;

        foreach ($cart['products'] as $product) {
            $amount += $product['price'] * $product['quantity'];
        }

        return $amount;
    }
}
