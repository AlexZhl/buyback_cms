<?php

namespace Corporation\UserAreaBundle\Manager;

use Knp\Component\Pager\Paginator;

class ProductCategoryManager extends BaseManager
{
    public function searchByName($searchTerm)
    {
        return $this->repository->createQueryBuilder('pc')
            ->where('pc.name LIKE :searchTerm')
            ->setParameter('searchTerm', '%' . $searchTerm . '%')
            ->getQuery()
            ->getResult()
            ;
    }
}
