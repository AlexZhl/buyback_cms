<?php

namespace Corporation\UserAreaBundle\Form;

use Corporation\UserAreaBundle\Form\Extension\StatesType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CustomerProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('addressLine1', TextType::class)
            ->add('addressLine2', TextType::class)
            ->add('city', TextType::class)
            ->add('region', StatesType::class)
//            ->add('region', TextType::class)
            ->add('postalCode', TextType::class)
            ->add('country', ChoiceType::class, [
                'choices' => [
                    'United States' => 'US',
                ],
                'choices_as_values' => true,
            ])
//            ->add('country', CountryType::class, [
//                'preferred_choices' => [ 'US', ],
//            ])
            ->add('phone', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Corporation\AdminAreaBundle\Entity\Customer',
//            'validation_groups' => [ 'profile_edit' ]
        ));
    }
}