<?php

namespace Corporation\UserAreaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BillingInfoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cardNumber', TextType::class)
            ->add('cardCVV', TextType::class)
            ->add('cardYear', TextType::class)
            ->add('cardMonth', TextType::class)
            ->add('billingSameAsShippingAddress', CheckboxType::class)
            ->add('billingFirstname', TextType::class)
            ->add('billingLastname', TextType::class)
            ->add('billingAddressLine1', TextType::class)
            ->add('billingAddressLine2', TextType::class)
            ->add('billingCity', TextType::class)
            ->add('billingRegion', TextType::class)
            ->add('billingPostalCode', TextType::class)
            ->add('billingCountry', CountryType::class, [
                'preferred_choices' => [ 'US', ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Corporation\AdminAreaBundle\Entity\Customer',
//            'validation_groups' => [ 'registration' ]
        ));
    }
}