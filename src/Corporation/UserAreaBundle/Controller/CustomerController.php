<?php

namespace Corporation\UserAreaBundle\Controller;

use Corporation\UserAreaBundle\Form\CustomerProfileFormType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */
class CustomerController extends Controller
{
    /**
     * @Route("/customer/pfoile", name="corporation.userarea.customer.profile")
     */
    public function profileAction(Request $request)
    {
        $customer = $this->getUser();

        $form = $this->createForm(CustomerProfileFormType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('doctrine.orm.entity_manager')->flush();
            $this->addFlash('notice', 'Changes saved');

            return $this->redirectToRoute('corporation.userarea.customer.profile');
        }

        return $this->render('@CorporationUserArea/Customer/profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
