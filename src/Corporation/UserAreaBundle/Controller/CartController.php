<?php

namespace Corporation\UserAreaBundle\Controller;

use Corporation\UserAreaBundle\Entity\CartProduct;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * @Route("/", name="corporation.userarea.cart.index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('@CorporationUserArea/Cart/index.html.twig', [
            'cart' => $this->get('corporation.userarea.manager.cart')->getCart(),
        ]);
    }

    /**
     * @Route("/add/{id}/{quantity}/", name="corporation.userarea.cart.add")
     */
    public function addAction(Request $request, $id = null, $quantity = null)
    {
        $product = $this->get('corporation.userarea.manager.product')->find($id);
        if (!$product) {
            return new JsonResponse(json_encode([
                'success' => false,
                'message' => 'Unable to find that product',
            ]));
        }

        $this->get('corporation.userarea.manager.cart')->addProduct($product, $quantity);

        return new JsonResponse(json_encode(['success' => true]));
    }

    /**
     * @Route("/remove/{id}/", name="corporation.userarea.cart.remove")
     */
    public function removeAction(Request $request, $id = null, $quantity = null)
    {
        $this->get('corporation.userarea.manager.cart')->removeProduct($id);

        return $this->redirectToRoute('corporation.userarea.cart.index');
    }
}
