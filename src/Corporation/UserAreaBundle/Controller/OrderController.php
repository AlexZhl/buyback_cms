<?php

namespace Corporation\UserAreaBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use Corporation\AdminAreaBundle\Entity\Customer;
use Corporation\AdminAreaBundle\Entity\Order;
use Corporation\UserAreaBundle\Form\BillingInfoFormType;
use Corporation\UserAreaBundle\Form\ShippingInfoFormType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/order")
 */
class OrderController extends Controller
{
    /**
     * @Route("/shipping", name="corporation.userarea.order.shipping")
     */
    public function shippingAction(Request $request)
    {
        $cart = $this->get('session')->get('cart', []);
        if ($cart && isset($cart['products']) && is_array($cart['products'])) {
        } else {
            $this->addFlash('error', 'Your cart is empty');
            return $this->redirectToRoute('corporation.userarea.shop.index');
        }

        $customer = $this->getUser();
        if (!$customer) {
            $customer = $this->get('session')->get('customer');
            if (!$customer) {
                $customer = new Customer();
                $this->get('session')->set('customer', $customer);
            }
        }

        $form = $this->createForm(ShippingInfoFormType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$this->getUser()) {
                $this->get('session')->set('customer', $customer);
            } else {
                $this->get('doctrine.orm.entity_manager')->flush();
            }

            return $this->redirectToRoute('corporation.userarea.order.shippingMethod');
        }

        return $this->render('@CorporationUserArea/Order/shipping.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/shipping-method", name="corporation.userarea.order.shippingMethod")
     */
    public function shippingMethodAction(Request $request)
    {
        $customer = $this->getUser();
        if (!$customer) {
            $customer = $this->get('session')->get('customer');
            if (!$customer) {
                return $this->redirectToRoute('corporation.userarea.order.shipping');
            }
        }

        $ratesData = $this->get('corporation.userarea.manager.usps')->calculateRates($customer);

        if (!$ratesData['success']) {
            $this->addFlash('error', $ratesData['message']);
            return $this->redirectToRoute('corporation.userarea.order.shipping');
        }

        return $this->render('@CorporationUserArea/Order/shipping_method.html.twig', [
            'rates' => $ratesData['rates'],
        ]);
    }

    /**
     * @Route("/payment", name="corporation.userarea.order.payment")
     */
    public function paymentAction(Request $request)
    {
        $cart = $this->get('corporation.userarea.manager.cart')->getCart();

        if ($cart['amount'] == 0) {
            $this->addFlash('error', 'Your cart is empty');
            return $this->redirectToRoute('corporation.userarea.shop.index');
        }

        $customer = $this->getUser();
        if (!$customer) {
            $customer = $this->get('session')->get('customer');
            if (!$customer) {
                return $this->redirectToRoute('corporation.userarea.order.shipping');
            }
        }

        $form = $this->createForm(BillingInfoFormType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($customer->getBillingSameAsShippingAddress()) {
                $customer->copyShippingToBilling();
            }

            $responseData = $this->get('corporation.userarea.manager.payment')->processPayment($customer, $cart['amount'], null);

            $customer->clearCardData();

            if (!$this->getUser()) {
                $this->get('session')->set('customer', $customer);
            } else {
                $this->get('doctrine.orm.entity_manager')->flush($customer);
            }

            if ($responseData['success']) {
                $this->addFlash('notice', $responseData['message']);

                $order = new Order($customer);
                if ($this->getUser()) {
                    $order->setCustomer($customer);
                }
                $order->setPaymentMethod(Order::PAYMENT_METHOD_AUTHORIZE_NET);
                $order->setTransactionNumber($responseData['transactionId']);
                $order->setAmount($cart['amount']);
                $order->setPaymentStatus(Order::PAYMENT_STATUS_PENDING);
                $order->setDeliveryMethod(Order::DELIVERY_METHOD_USPS_DOMESTIC_EXPRESS);
                $order->setDeliveryStatus(Order::DELIVERY_STATUS_OPEN);
                $order->setStatus(Order::ORDER_STATUS_OPEN);

                $this->get('doctrine.orm.entity_manager')->persist($order);
                $this->get('doctrine.orm.entity_manager')->flush($order);

//                return $this->redirectToRoute('corporation.userarea.order.payment_success');
            } else {
                $this->addFlash('error', $responseData['message']);
            }
        }

        return $this->render('@CorporationUserArea/Order/payment.html.twig', [
            'form' => $form->createView(),
            'amount' => $cart['amount'],
        ]);
    }

    /**
     * @Route("/payment-success", name="corporation.userarea.order.payment_success")
     */
    public function paymentSuccess(Request $request, $paypalSuccess)
    {
        return $this->render('@CorporationUserArea/Order/payment_success.html.twig', []);
    }
}
