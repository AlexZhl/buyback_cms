<?php

namespace Corporation\UserAreaBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    const MAX_POPULAR_PRODUCTS = 6;

    /**
     * @Route("/", name="corporation.userarea.main.index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('@CorporationUserArea/Main/index.html.twig', [
            'products' => $this->get('corporation.userarea.manager.product')->findBy(['popular' => true]),
        ]);
    }

    /**
     * @Route("/about-us", name="corporation.userarea.main.about")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('@CorporationUserArea/Main/about.html.twig', []);
    }

    /**
     * @Route("/contact-us", name="corporation.userarea.main.contact")
     */
    public function contactAction(Request $request)
    {
        return $this->render('@CorporationUserArea/Main/contact.html.twig', []);
    }

    /**
     * @Route("/search", name="corporation.userarea.main.search")
     */
    public function searchAction(Request $request)
    {
        $searchTerm = $request->get('term');

        $products = $this->get('corporation.userarea.manager.product')->searchByTitle($searchTerm);

        $productsByCategories = [];
        $productCategories = $this->get('corporation.userarea.manager.product_category')->searchByName($searchTerm);
        foreach ($productCategories as $productCategory) {
            $productCategories[] = $productCategory->getProducts();
        }

        $products = array_unique(array_merge($products, $productsByCategories));

        return $this->render('@CorporationUserArea/Main/search.html.twig', [
            'products' => $products,
        ]);
    }
}
