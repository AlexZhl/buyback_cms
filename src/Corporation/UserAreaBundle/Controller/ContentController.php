<?php

namespace Corporation\UserAreaBundle\Controller;

use Corporation\AdminAreaBundle\Entity\Question;
use Corporation\AdminAreaBundle\Entity\Review;
use Corporation\UserAreaBundle\Form\QuestionFormType;
use Corporation\UserAreaBundle\Form\ReviewFormType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContentController extends Controller
{
    const RECIPES_PER_PAGE = 10;
    const REVIEWS_PER_PAGE = 10;
    const QUESTIONS_PER_PAGE = 10;

    /**
     * @Route("/recipes", name="corporation.userarea.content.recipes")
     */
    public function recipesAction(Request $request)
    {
        $page = $request->request->getInt('page', 1);
        $itemsPerPage = $request->request->getInt('itemsPerPage', self::RECIPES_PER_PAGE);

        $items = $this->get('corporation.userarea.manager.recipe')->getItemsWithPagination($page, $itemsPerPage);

        return $this->render('@CorporationUserArea/Content/recipes.html.twig', [
            'recipes' => $items,
        ]);
    }

    /**
     * @Route("/recipes/{slug}", name="corporation.userarea.content.recipe")
     */
    public function recipeAction(Request $request, $slug)
    {
        if (!$item = $this->get('corporation.userarea.manager.recipe')->findOneBy(['slug' => $slug])) {
            $this->addFlash('error', 'Unable to find that recipe');
            return $this->redirectToRoute('corporation.userarea.content.recipes');
        }

        return $this->render('@CorporationUserArea/Content/recipe.html.twig', [
            'recipe' => $item,
        ]);
    }

    /**
     * @Route("/reviews", name="corporation.userarea.content.reviews")
     */
    public function reviewsAction(Request $request)
    {
        $page = $request->request->getInt('page', 1);
        $itemsPerPage = $request->request->getInt('itemsPerPage', self::REVIEWS_PER_PAGE);

        $items = $this->get('corporation.userarea.manager.review')->getItemsWithPagination($page, $itemsPerPage);

        $review = new Review();
        $form = $this->createForm(ReviewFormType::class, $review);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('doctrine.orm.entity_manager')->persist($review);
            $this->get('doctrine.orm.entity_manager')->flush();

            $this->addFlash('notice', 'Thank you for your reivew');

            return $this->redirectToRoute('corporation.userarea.content.reviews');
        }

        return $this->render('@CorporationUserArea/Content/reviews.html.twig', [
            'reviews' => $items,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/faq", name="corporation.userarea.content.faq")
     */
    public function faqAction(Request $request)
    {
        $page = $request->request->getInt('page', 1);
        $itemsPerPage = $request->request->getInt('itemsPerPage', self::QUESTIONS_PER_PAGE);

        $items = $this->get('corporation.userarea.manager.question')->getItemsWithPagination($page, $itemsPerPage);

        $question = new Question();
        $customer = $this->getUser();
        if ($customer) {
            $question->setEmail($customer->getEmail());
        }

        $form = $this->createForm(QuestionFormType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('doctrine.orm.entity_manager')->persist($question);
            $this->get('doctrine.orm.entity_manager')->flush();

            $this->addFlash('notice', 'Thank you. You will get answer as soon as possible');

            return $this->redirectToRoute('corporation.userarea.content.faq');
        }

        return $this->render('@CorporationUserArea/Content/faq.html.twig', [
            'questions' => $items,
            'form' => $form->createView(),
        ]);
    }
}
