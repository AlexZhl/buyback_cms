<?php

namespace Corporation\UserAreaBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends Controller
{
    const ITEMS_PER_PAGE = 20;

    /**
     * @Route("/shop", name="corporation.userarea.shop.index")
     */
    public function indexAction(Request $request)
    {
        $category = $request->get('category', null);

        $page = $request->request->getInt('page', 1);
        $itemsPerPage = $request->request->getInt('itemsPerPage', self::ITEMS_PER_PAGE);

        return $this->render('@CorporationUserArea/Shop/index.html.twig', [
            'products' => $this->get('corporation.userarea.manager.product')->getProductsWithPagination($page, $itemsPerPage, $category),
            'categories' => $this->get('corporation.userarea.manager.product_category')->findAll(),
        ]);
    }

    /**
     * @Route("/shop/product/{slug}", name="corporation.userarea.shop.product")
     */
    public function productAction(Request $request, $slug)
    {
        if (!$product = $this->get('corporation.userarea.manager.product')->findOneBy(['slug' => $slug])) {
            $this->addFlash('error', 'Unable to find that product.');
            return $this->redirectToRoute('corporation.userarea.shop.index');
        }

        return $this->render('@CorporationUserArea/Shop/product.html.twig', [
            'product' => $product,
        ]);
    }
}
