<?php

namespace Corporation\UserAreaBundle\Controller;

use Corporation\AdminAreaBundle\Entity\Customer;
use Corporation\UserAreaBundle\Form\ChangePasswordFormType;
use Corporation\UserAreaBundle\Form\ForgotPasswordFormType;
use Corporation\UserAreaBundle\Form\RegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="corporation.userarea.security.login")
     */
    public function loginAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('corporation.userarea.main.index');
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        if ($error) {
            $this->addFlash('error', $error->getMessageKey());
        }
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@CorporationUserArea/Security/login.html.twig', [
            'last_username' => $lastUsername,
        ]);
    }

    /**
     * @Route("/register", name="corporation.userarea.security.register")
     */
    public function registerAction(Request $request, $formLess = false)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('corporation.userarea.shop.index');
        }

        $customer = new Customer();
        $form = $this->createForm(RegistrationFormType::class, $customer);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')->encodePassword($customer, $customer->getPlainPassword());
            $customer->setPassword($password);
            $em = $this->get('doctrine')->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->redirectToRoute('corporation.userarea.security.login');
        }

        foreach ($form->getErrors(true) as $formError) {
            $this->addFlash('error', $formError->getMessage());
        }

        $template = $formLess ? '@CorporationUserArea/Security/register_less.html.twig' : '@CorporationUserArea/Security/register.html.twig';
        return $this->render($template, [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/forgot-password/", name="corporation.userarea.security.forgot")
     */
    public function forgotAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('corporation.userarea.main.index');
        }

        $customer = new Customer();
        $form = $this->createForm(ForgotPasswordFormType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $this
                ->getDoctrine()
                ->getRepository(Customer::class)
                ->findOneBy([ 'email' => $customer->getEmail()]);

            if (!$customer) {
                $this->addFlash('error', 'That email address doesn\'t match any user accounts. ');
                return $this->render('@CorporationUserArea/Security/forgot_password.html.twig', [
                    'form' => $form->createView(),
                ]);
            }

            $customer->setResetPasswordToken(md5($this->getParameter('secret') . time()));
            $this->get('doctrine.orm.entity_manager')->flush();

            $this->get('corporation.adminarea.manager.mail')->sendMail([
                'email' => $customer->getEmail(),
                'subject' => 'Password recovery',
                'body' => $this->get('templating')->render('@CorporationUserArea/Mail/forgot_password.html.twig', [
                    'customer' => $customer,
                ]),
            ]);

            $this->addFlash(
                'notice',
                'To reset your password, follow the instructions in the email we\'ve just sent you.'
            );
            return $this->render('@CorporationUserArea/Security/forgot_password.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        return $this->render('@CorporationUserArea/Security/forgot_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/reset-password/{resetPasswordToken}", name="corporation.userarea.security.resetPassword")
     */
    public function resetPasswordAction($resetPasswordToken, Request $request)
    {
        $customer = $this
            ->getDoctrine()
            ->getRepository(Customer::class)
            ->findOneBy([ 'resetPasswordToken' => $resetPasswordToken ]);

        if (!$customer || !$customer->isResetPasswordTokenValid()) {
            $this->addFlash('error', 'Invalid link');
            return $this->redirectToRoute('corporation.userarea.security.forgot');
        }

        $form = $this->createForm(ChangePasswordFormType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')->encodePassword($customer, $customer->getPlainPassword());
            $customer->setPassword($password);
            $customer->setResetPasswordToken(null);
            $this->get('doctrine.orm.entity_manager')->flush();
            $this->addFlash('notice', 'Password recovered successfully! You can now log in.');
            return $this->redirectToRoute('corporation.userarea.security.login');
        }

        return $this->render('@CorporationUserArea/Security/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="corporation.userarea.security.logout")
     */
    public function logoutAction(Request $request)
    {
        return $this->redirectToRoute('corporation.userarea.main.index');
    }
}