<?php

namespace Corporation\AdminAreaBundle\DataFixtures\ORM;

use Corporation\AdminAreaBundle\Entity\Region;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRegions extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $regionFl = new Region();
        $regionFl->setName('Florida, USA');
        $manager->persist($regionFl);

        $regionNY = new Region();
        $regionNY->setName('New York, USA');
        $manager->persist($regionNY);

        $manager->flush();
    }
}
