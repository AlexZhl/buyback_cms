<?php

namespace Corporation\AdminAreaBundle\DataFixtures\ORM;

use Corporation\AdminAreaBundle\Entity\Place;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadPlaces extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
//        $sellingPlacesData = json_decode(file_get_contents($this->getImportFile('google-places-selling-list')));
//        $this->processData($manager, $sellingPlacesData, Place::PLACE_TYPE_SELLING);

//        $sellingPlacesData = json_decode(file_get_contents($this->getImportFile('google-places-confused-list')));
//        $this->processData($manager, $sellingPlacesData, Place::PLACE_TYPE_CONFUSED);

//        $sellingPlacesData = json_decode(file_get_contents($this->getImportFile('google-places-doesnt-sell-list')));
//        $this->processData($manager, $sellingPlacesData, Place::PLACE_TYPE_DOESNT_SELL);

//        $sellingPlacesData = json_decode(file_get_contents($this->getImportFile('google-places-to-go-list')));
//        $this->processData($manager, $sellingPlacesData, Place::PLACE_TYPE_WANT_TO_GO);
    }

    public function processData(ObjectManager $manager, $placesData, $placeType)
    {
        $day = new \DateTime('now-2 day');

        foreach ($placesData->features as $placeData) {
            try {
                $placeLocation = $placeData->properties->Location;

                $place = new Place();
                if (isset($placeLocation->{'Business Name'})) {
                    $place->setName($placeLocation->{'Business Name'});
                    $place->setAddress($placeLocation->Address);
                    $place->setLat($placeLocation->{'Geo Coordinates'}->Latitude);
                    $place->setLng($placeLocation->{'Geo Coordinates'}->Longitude);
                } else {
                    $place->setName('No Name');
                    $place->setAddress($placeData->properties->Title);
                    $place->setLat($placeLocation->Latitude);
                    $place->setLng($placeLocation->Longitude);
                }
                $place->setEnabled(true);
                $place->setType($placeType);
                if ($placeType == Place::PLACE_TYPE_SELLING) {
                    $place->setLastVisit($day);
                }
                $place->setNewScreensEachDay(rand(2, 46));
                $manager->persist($place);
            } catch (\Exception $exception) {
                dump($placeData);
                dump($exception->getMessage());
                throw new \Exception();
            }
        }

        $manager->flush();
    }
}
