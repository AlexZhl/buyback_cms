<?php

namespace Corporation\AdminAreaBundle\DataFixtures\ORM;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAdmins extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setFirstName('Admin');
        $admin->setLastname('Test');
        $admin->setPlainPassword('12345678');
        $admin->setUsername('admin@test.com');
        $admin->setUsernameCanonical('admin@test.com');
        $admin->setEmail('admin@test.com');
        $admin->setEmailCanonical('admin@test.com');
        if ($this->container->has('sonata.user.google.authenticator.provider')) {
            $admin->setTwoStepVerificationCode(
                $this->container->get('sonata.user.google.authenticator.provider')->generateSecret()
            );
        }
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setEnabled(true);
        $manager->persist($admin);

        $manager->flush();
    }
}
