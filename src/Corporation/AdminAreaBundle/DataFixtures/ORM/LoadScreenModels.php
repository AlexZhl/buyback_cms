<?php

namespace Corporation\AdminAreaBundle\DataFixtures\ORM;

use Corporation\AdminAreaBundle\Entity\ScreenModel;
use Doctrine\Common\Persistence\ObjectManager;

class LoadScreenModels extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone X');
        $screenModel->setDefaultBadPrice(2);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 8 Plus');
        $screenModel->setDefaultBadPrice(2);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 8');
        $screenModel->setDefaultBadPrice(2);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 7 Plus');
        $screenModel->setDefaultBadPrice(1);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 7');
        $screenModel->setDefaultBadPrice(1);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 6S Plus');
        $screenModel->setDefaultBadPrice(0.50);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 6S');
        $screenModel->setDefaultBadPrice(0.50);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 6 Plus');
        $screenModel->setDefaultBadPrice(0.10);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 6');
        $screenModel->setDefaultBadPrice(0.10);
        $manager->persist($screenModel);

        $screenModel = new ScreenModel();
        $screenModel->setName('iPhone 5 series');
        $screenModel->setDefaultBadPrice(0.01);
        $manager->persist($screenModel);

        $manager->flush();
    }
}
