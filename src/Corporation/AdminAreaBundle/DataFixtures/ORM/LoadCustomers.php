<?php

namespace Corporation\AdminAreaBundle\DataFixtures\ORM;

use Corporation\AdminAreaBundle\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCustomers extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $customer = new Customer();
        $customer->setFullName('Ivan Ivanov');
        $customer->setEmail('customer@test.com');
        $customer->setIsActive(true);
        $customer->setTrusted(false);
        $customer->setPassword($this->container->get('security.password_encoder')->encodePassword($customer, '123'));

        $manager->persist($customer);
        $manager->flush();
    }
}
