<?php

namespace Corporation\AdminAreaBundle\Form\Type;

use Corporation\AdminAreaBundle\Manager\ScreenModelManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ScreenModelsQuantityType extends AbstractType
{
    private $screenModelManager;

    public function __construct(ScreenModelManager $screenModelManager)
    {
        $this->screenModelManager = $screenModelManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Edit "screen_models_quantity_widget" in "app/Resources/CorporationAdminAreaBundle/views/Form/fields.html.twig"
        // if you are going to change this

        $screenModels = $this->screenModelManager->findAll();

        foreach ($screenModels as $screenModel) {
            $builder
                ->add($screenModel->getSysName() . '_oem_price', TextType::class, ['attr' => [
                    'labelText' => 'OEM Price ($)',
                    'value' => $screenModel->getDefaultOemPrice(),
                    'model' => $screenModel->getName(),
                    'calculated-data' => 'oem-price',
                    'class' => 'price-calculated',
                ]])
                ->add($screenModel->getSysName() . '_oem', TextType::class, ['required' => false, 'attr' => [
                    'labelText' => 'OEM Quantity',
                    'model' => $screenModel->getName(),
                    'price-selector' => 'oem-price',
                    'class' => 'quantity-calculated',
                ]])
                ->add($screenModel->getSysName() . '_issues_price', TextType::class, ['attr' => [
                    'labelText' => 'Issues Price ($)',
                    'model' => $screenModel->getName(),
                    'value' => $screenModel->getDefaultIssuesPrice(),
                    'calculated-data' => 'issues-price',
                    'class' => 'price-calculated',
                ]])
                ->add($screenModel->getSysName() . '_issues', TextType::class, ['required' => false, 'attr' => [
                    'labelText' => 'Issues Quantity',
                    'model' => $screenModel->getName(),
                    'price-selector' => 'issues-price',
                    'class' => 'quantity-calculated',
                ]])
            ;
        }

        foreach ($screenModels as $screenModel) {
            $builder
                ->add($screenModel->getSysName() . '_bad_price', TextType::class, ['attr' => [
                    'labelText' => 'Price ($)',
                    'model' => $screenModel->getName() . ' Bad',
                    'value' => $screenModel->getDefaultBadPrice(),
                    'calculated-data' => 'bad-price',
                    'class' => 'price-calculated',
                ]])
                ->add($screenModel->getSysName() . '_bad', TextType::class, ['required' => false, 'attr' => [
                    'labelText' => 'Quantity',
                    'model' => $screenModel->getName() . ' Bad',
                    'price-selector' => 'bad-price',
                    'class' => 'quantity-calculated',
                ]])
            ;
        }

        $builder
            ->add('custom_description_price', TextType::class, ['required' => false, 'attr' => [
                'labelText' => 'Custom Amount ($)',
                'class' => 'price-calculated',
                'calculated-data' => 'custom-price',
                'model' => '',
            ]])
            ->add('custom_description', TextType::class, ['required' => false, 'attr' => [
                'labelText' => 'Description',
                'model' => '',
            ]])
        ;
    }
}
