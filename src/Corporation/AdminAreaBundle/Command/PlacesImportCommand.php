<?php

namespace Corporation\AdminAreaBundle\Command;

use Corporation\AdminAreaBundle\Entity\Place;
use Corporation\AdminAreaBundle\Entity\Region;
use Corporation\AdminAreaBundle\Manager\PlaceManager;
use Corporation\AdminAreaBundle\Manager\RegionManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PlacesImportCommand extends Command
{
    protected $placeManager;
    protected $regionManager;

    public function __construct(PlaceManager $placeManager, RegionManager $regionManager)
    {
        $this->placeManager = $placeManager;
        $this->regionManager = $regionManager;
        parent::__construct();
    }

    protected function configure()
    {
        // try to avoid work here (e.g. database query)
        // this method is *always* called - see warning below

        $this
            ->setName('corporation:places:import')
            ->setDescription('Import Places data from the Google Places files.')
            ->addOption('filename', 'f', InputOption::VALUE_REQUIRED, 'Specific filename with places data (Google Places JSON)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getOption('filename');
        $placesData = json_decode(file_get_contents($filename));

        foreach ($placesData as $placeData) {
            $output->writeln(sprintf('Loading Place "%s"', $placeData->name));
            $place = new Place();
            // Trick to avoid Geocoder Request and avoid Geocoder daily limits
            $place->setAvoidGeocoderRequest(true);
            $place->setName($placeData->name);
            $place->setAddress($placeData->address);
            $place->setPhone($placeData->phone);
            $place->setLat($placeData->lat);
            $place->setLng($placeData->lng);
            $place->setNewScreensEachDay($placeData->newScreensEachDay);
            if ($placeData->lastVisit) {
                $place->setLastVisit(new \DateTime($placeData->lastVisit));
            }
            $place->setType($placeData->type);
            $place->setNotes($placeData->notes);
            $place->setEnabled($placeData->enabled);
            $place->setLastPurchaseTotal($placeData->lastPurchaseTotal);
            $place->setLastPurchaseScreens($placeData->lastPurchaseScreens);

            $region = $this->regionManager->findOneBy(['name' => $placeData->{'region.name'}]);
            if (!$region) {
                $region = new Region();
                $region->setName($placeData->{'region.name'});
                $this->regionManager->saveEntity($region);
            }
            $place->setRegion($region);

            $this->placeManager->saveEntity($place);
        }

        $output->writeln('Done');
    }
}