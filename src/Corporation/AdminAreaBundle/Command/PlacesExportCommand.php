<?php

namespace Corporation\AdminAreaBundle\Command;

use Corporation\AdminAreaBundle\Manager\PlaceManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PlacesExportCommand extends Command
{
    protected $placeManager;

    public function __construct(PlaceManager $placeManager)
    {
        $this->placeManager = $placeManager;
        parent::__construct();
    }

    protected function configure()
    {
        // try to avoid work here (e.g. database query)
        // this method is *always* called - see warning below

        $this
            ->setName('corporation:places:export')
            ->setDescription('Export Places data (JSON format).')
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL, 'Export Places by Type')
            ->addOption('filename', 'f', InputOption::VALUE_OPTIONAL, 'Specify file location which to export places data to')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getOption('type');
        $filename = $input->getOption('filename');

        $output->writeln('Done');
    }
}