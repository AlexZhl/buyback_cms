<?php

namespace Corporation\AdminAreaBundle\Command;

use Corporation\AdminAreaBundle\Manager\MailManager;
use Corporation\AdminAreaBundle\Manager\PlaceManager;
use Sonata\UserBundle\Entity\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class VisitPlacesReminderCommand extends Command
{
    protected $placeManager;

    public function __construct(PlaceManager $placeManager, MailManager $mailManager, UserManager $userManager)
    {
        $this->placeManager = $placeManager;
        $this->mailManager = $mailManager;
        $this->userManager = $userManager;

        parent::__construct();
    }

    protected function configure()
    {
        // try to avoid work here (e.g. database query)
        // this method is *always* called - see warning below

        $this
            ->setName('corporation:places:visit-reminder')
            ->setDescription('Remind employees via email to visit specific stores.')
            ->addOption(
                'user-email',
                '-u',
                InputOption::VALUE_OPTIONAL,
                'Specific person to remind'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userEmail = $input->getOption('user-email');
        $user = $this->userManager->findUserByEmail($input->getOption('user-email'));

        $placesToVisit = $this->placeManager->findPlacesToVisit($user);

        $output->writeln('Done');
    }
}