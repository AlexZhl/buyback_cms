<?php

namespace Corporation\AdminAreaBundle\Block;

use Corporation\AdminAreaBundle\Manager\PurchaseManager;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Model\Metadata;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TopEmployeesBlock extends AbstractAdminBlockService
{
    protected $purchaseManager;

    public function __construct($name, EngineInterface $templating, PurchaseManager $purchaseManager)
    {
        parent::__construct($name, $templating);
        $this->purchaseManager = $purchaseManager;
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'month' => false,
            'max_results' => '5',
            'title' => 'Today\'s Top Employees',
            'template' => 'CorporationAdminAreaBundle:Block:block_top_employees.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('month', 'boolean', array('required' => false)),
                array('max_results', 'text', array('required' => false)),
                array('title', 'text', array('required' => false)),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        $errorElement
            ->with('settings[month]')
            ->assertNotNull(array())
            ->end()
            ->with('settings[max_results]')
            ->assertNotNull(array())
            ->assertNotBlank()
            ->assertLength(array('max' => 50))
            ->end()
            ->with('settings[title]')
            ->assertNotNull(array())
            ->assertNotBlank()
            ->assertLength(array('max' => 50))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        // merge settings
        $settings = $blockContext->getSettings();

        return $this->renderResponse($blockContext->getTemplate(), array(
            'results' => $this->purchaseManager->findTopEmployees($settings['max_results'], $settings['month']),
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockMetadata($code = null)
    {
        return new Metadata($this->getName(), (!is_null($code) ? $code : $this->getName()), false, 'SonataBlockBundle', array(
            'class' => 'fa fa-rss-square',
        ));
    }
}
