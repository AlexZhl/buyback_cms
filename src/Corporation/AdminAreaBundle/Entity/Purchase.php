<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="purchases")
 * @ORM\Entity
 * @ORM\EntityListeners({"Corporation\AdminAreaBundle\EventListener\PurchaseListener"})
 */
class Purchase
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // TODO: Create beautifull way to handle this. (See "Corporation\AdminAreaBundle\EventListener\PurchaseListener")
    protected $screenModels;

    /**
     * @ORM\OneToMany(targetEntity="PurchaseElement", mappedBy="purchase", cascade={"persist", "remove"})
     */
    private $purchaseElements;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_amount", type="decimal", scale=2, nullable=true)
     */
    private $customAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", scale=2, nullable=true)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="voided", type="boolean")
     */
    private $voided;

    /**
     * @var string
     *
     * @ORM\Column(name="total_screens", type="integer", nullable=true)
     */
    private $totalScreens;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_amount_description", type="string", length=255, nullable=true)
     */
    private $customAmountDescription;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="purchaser_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $purchaser;

    /**
     * @ORM\ManyToOne(targetEntity="\Corporation\AdminAreaBundle\Entity\Place", cascade={"persist"}, inversedBy="purchases")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $place;

    /**
     * @ORM\Column(name="customer_email", type="string", length=255)
     */
    private $customerEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signature_image", type="blob")
     */
    private $signatureImage;

    function __construct()
    {
        $this->voided = false;
        $this->setCreatedAt(new \DateTime());
        $this->purchaseElements = new ArrayCollection();
    }

    public function getPurchaserName()
    {
        return $this->getPurchaser()->getFullname();
    }

    public function recalculateTotal()
    {
        $total = 0 + $this->getCustomAmount();
        $totalScreens = 0;

        foreach ($this->purchaseElements as $purchaseElement) {
            $total += $purchaseElement->getTotal();
            $totalScreens += $purchaseElement->getTotalScreens();
        }

        $this->setTotal($total);
        $this->setTotalScreens($totalScreens);
    }

    public function getTotalWithBad()
    {
        $total = 0 + $this->getCustomAmount();

        foreach ($this->purchaseElements as $purchaseElement) {
            $total += $purchaseElement->getTotalWithBad();
        }

        return $total;
    }

    public function getTotalScreensWithBad()
    {
        $totalScreens = 0;

        foreach ($this->purchaseElements as $purchaseElement) {
            $totalScreens += $purchaseElement->getTotalScreensWithBad();
        }

        return $totalScreens;
    }

    public function getSignatureImageAsHtmlImg()
    {
        return sprintf('<img src="%s" class="img-responsive">', stream_get_contents($this->getSignatureImage()));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set screenModels
     *
     * @param json $screenModels
     * @return Purchase
     */
    public function setScreenModels($screenModels)
    {
        $this->screenModels = $screenModels;

        return $this;
    }

    /**
     * Get screenModels
     *
     * @return json 
     */
    public function getScreenModels()
    {
        return $this->screenModels;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Purchase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set purchaser
     *
     * @param \Application\Sonata\UserBundle\Entity\User $purchaser
     * @return Purchase
     */
    public function setPurchaser(\Application\Sonata\UserBundle\Entity\User $purchaser = null)
    {
        $this->purchaser = $purchaser;

        return $this;
    }

    /**
     * Get purchaser
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getPurchaser()
    {
        return $this->purchaser;
    }

    /**
     * Set place
     *
     * @param \Corporation\AdminAreaBundle\Entity\Place $place
     * @return Purchase
     */
    public function setPlace(\Corporation\AdminAreaBundle\Entity\Place $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \Corporation\AdminAreaBundle\Entity\Place 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set signatureImage
     *
     * @param string $signatureImage
     * @return Purchase
     */
    public function setSignatureImage($signatureImage)
    {
        $this->signatureImage = $signatureImage;

        return $this;
    }

    /**
     * Get signatureImage
     *
     * @return string 
     */
    public function getSignatureImage()
    {
        return $this->signatureImage;
    }

    /**
     * Set customerEmail
     *
     * @param string $customerEmail
     * @return Purchase
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    /**
     * Get customerEmail
     *
     * @return string 
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * Add purchaseElements
     *
     * @param \Corporation\AdminAreaBundle\Entity\PurchaseElement $purchaseElements
     * @return Purchase
     */
    public function addPurchaseElement(\Corporation\AdminAreaBundle\Entity\PurchaseElement $purchaseElements)
    {
        $this->purchaseElements[] = $purchaseElements;

        $this->recalculateTotal();

        return $this;
    }

    /**
     * Remove purchaseElements
     *
     * @param \Corporation\AdminAreaBundle\Entity\PurchaseElement $purchaseElements
     */
    public function removePurchaseElement(\Corporation\AdminAreaBundle\Entity\PurchaseElement $purchaseElements)
    {
        $this->purchaseElements->removeElement($purchaseElements);
        $this->recalculateTotal();
    }

    /**
     * Get purchaseElements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchaseElements()
    {
        return $this->purchaseElements;
    }

    /**
     * Set customAmount
     *
     * @param string $customAmount
     * @return Purchase
     */
    public function setCustomAmount($customAmount)
    {
        $this->customAmount = $customAmount;

        return $this;
    }

    /**
     * Get customAmount
     *
     * @return string 
     */
    public function getCustomAmount()
    {
        return $this->customAmount;
        $this->recalculateTotal();
    }

    /**
     * Set customAmountDescription
     *
     * @param string $customAmountDescription
     * @return Purchase
     */
    public function setCustomAmountDescription($customAmountDescription)
    {
        $this->customAmountDescription = $customAmountDescription;

        return $this;
    }

    /**
     * Get customAmountDescription
     *
     * @return string 
     */
    public function getCustomAmountDescription()
    {
        return $this->customAmountDescription;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Purchase
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set totalScreens
     *
     * @param integer $totalScreens
     * @return Purchase
     */
    public function setTotalScreens($totalScreens)
    {
        $this->totalScreens = $totalScreens;

        return $this;
    }

    /**
     * Get totalScreens
     *
     * @return integer 
     */
    public function getTotalScreens()
    {
        return $this->totalScreens;
    }

    /**
     * Set voided
     *
     * @param boolean $voided
     * @return Purchase
     */
    public function setVoided($voided)
    {
        $this->voided = $voided;

        return $this;
    }

    /**
     * Get voided
     *
     * @return boolean 
     */
    public function getVoided()
    {
        return $this->voided;
    }
}
