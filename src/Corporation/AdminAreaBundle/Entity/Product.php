<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Product
 *
 * @ORM\Table(name="products")
 * @ORM\Entity
 * @UniqueEntity(fields="slug", message="Slug is already taken")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var float
     *
     * @ORM\Column(name="ounces", type="float")
     */
    private $ounces;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @var bool
     *
     * @ORM\Column(name="popular", type="boolean")
     */
    private $popular;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="main_photo", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $mainPhoto;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Gallery
     *
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Gallery", cascade={"persist"})
     * @ORM\JoinColumn(name="photo_gallery", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $photoGallery;

    /**
     * @ORM\ManyToMany(targetEntity="ProductCategory", inversedBy="products")
     * @ORM\JoinTable(
     *     name="products_to_categories",
     *     joinColumns={
     *      @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *  },
     *     inverseJoinColumns={
     *      @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     *  })
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Recipe", mappedBy="products")
     */
    private $recipes;

    /**
     * @ORM\ManyToMany(targetEntity="Question", mappedBy="products")
     */
    private $questions;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->recipes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        $this->setSlug($title);

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Product
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Product
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add categories
     *
     * @param \Corporation\AdminAreaBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function addCategory(\Corporation\AdminAreaBundle\Entity\ProductCategory $category)
    {
        if ($this->categories->contains($category)) {
            return $this;
        }
        $this->categories->add($category);
        $category->addProduct($this);

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Corporation\AdminAreaBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function removeCategory(\Corporation\AdminAreaBundle\Entity\ProductCategory $category)
    {
        if (!$this->categories->contains($category)) {
            return $this;
        }
        $this->categories->removeElement($category);
        $category->removeProduct($this);

        return $this;
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = Urlizer::urlize($slug, '-');

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set mainPhoto
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $mainPhoto
     * @return Product
     */
    public function setMainPhoto(\Application\Sonata\MediaBundle\Entity\Media $mainPhoto = null)
    {
        $this->mainPhoto = $mainPhoto;

        return $this;
    }

    /**
     * Get mainPhoto
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMainPhoto()
    {
        return $this->mainPhoto;
    }

    /**
     * Set photoGallery
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $photoGallery
     * @return Product
     */
    public function setPhotoGallery(\Application\Sonata\MediaBundle\Entity\Gallery $photoGallery = null)
    {
        $this->photoGallery = $photoGallery;

        return $this;
    }

    /**
     * Get photoGallery
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery
     */
    public function getPhotoGallery()
    {
        return $this->photoGallery;
    }

    /**
     * Add recipe
     *
     * @param \Corporation\AdminAreaBundle\Entity\Recipe $recipe
     * @return Product
     */
    public function addRecipe(\Corporation\AdminAreaBundle\Entity\Recipe $recipe)
    {
        if ($this->recipes->contains($recipe)) {
            return $this;
        }
        $this->recipes->add($recipe);
        $recipe->addProduct($this);

        return $this;
    }

    /**
     * Remove recipe
     *
     * @param \Corporation\AdminAreaBundle\Entity\Recipe $recipe
     * @return Product
     */
    public function removeRecipe(\Corporation\AdminAreaBundle\Entity\Recipe $recipe)
    {
        if (!$this->recipes->contains($recipe)) {
            return $this;
        }
        $this->recipes->removeElement($recipe);
        $recipe->removeProduct($this);

        return $this;
    }

    /**
     * Get recipes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    /**
     * Add question
     *
     * @param \Corporation\AdminAreaBundle\Entity\Question $question
     * @return Question
     */
    public function addQuestion(\Corporation\AdminAreaBundle\Entity\Question $question)
    {
        if ($this->questions->contains($question)) {
            return $this;
        }
        $this->questions->add($question);
        $question->addProduct($this);

        return $this;
    }

    /**
     * Remove question
     *
     * @param \Corporation\AdminAreaBundle\Entity\Question $question
     * @return Question
     */
    public function removeQuestion(\Corporation\AdminAreaBundle\Entity\Question $question)
    {
        if (!$this->questions->contains($question)) {
            return $this;
        }
        $this->questions->removeElement($question);
        $question->removeProduct($this);

        return $this;
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set popular
     *
     * @param boolean $popular
     * @return Product
     */
    public function setPopular($popular)
    {
        $this->popular = $popular;

        return $this;
    }

    /**
     * Get popular
     *
     * @return boolean 
     */
    public function getPopular()
    {
        return $this->popular;
    }

    /**
     * Set ounces
     *
     * @param float $ounces
     * @return Product
     */
    public function setOunces($ounces)
    {
        $this->ounces = $ounces;

        return $this;
    }

    /**
     * Get ounces
     *
     * @return float 
     */
    public function getOunces()
    {
        return $this->ounces;
    }
}
