<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="shippings")
 * @ORM\Entity()
 */
class Shipping
{
    const RESET_PASSWORD_TOKEN_VALID_TIME = 86400; // 24hrs in sec

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="approved", type="boolean")
     */
    private $approved;

    /**
     * @ORM\OneToMany(targetEntity="ShippingLabel", mappedBy="shipping")
     */
    private $shippingLabels;

    /**
     * @var string
     *
     * @ORM\Column(name="indentification_number", type="string", length=255, nullable=true)
     */
    private $identificationNumber;

    /**
     * @ORM\Column(name="charges", type="decimal", precision=8, scale=2, nullable=true)
     */
    private $charges;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="shippings")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address_line_1", type="string", length=255, nullable=true)
     */
    private $addressLine1;

    /**
     * @var string
     *
     * @ORM\Column(name="address_line_2", type="string", length=255, nullable=true)
     */
    private $addressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    private $shippingLabelsQuantity;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->shippingLabels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return Shipping
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set identificationNumber
     *
     * @param string $identificationNumber
     * @return Shipping
     */
    public function setIdentificationNumber($identificationNumber)
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    /**
     * Get identificationNumber
     *
     * @return string 
     */
    public function getIdentificationNumber()
    {
        return $this->identificationNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Shipping
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return Shipping
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set addressLine1
     *
     * @param string $addressLine1
     * @return Shipping
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    /**
     * Get addressLine1
     *
     * @return string 
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * Set addressLine2
     *
     * @param string $addressLine2
     * @return Shipping
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    /**
     * Get addressLine2
     *
     * @return string 
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Shipping
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Shipping
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Shipping
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Shipping
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Shipping
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Shipping
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add shippingLabels
     *
     * @param \Corporation\AdminAreaBundle\Entity\ShippingLabel $shippingLabels
     * @return Shipping
     */
    public function addShippingLabel(\Corporation\AdminAreaBundle\Entity\ShippingLabel $shippingLabels)
    {
        $this->shippingLabels[] = $shippingLabels;

        return $this;
    }

    /**
     * Remove shippingLabels
     *
     * @param \Corporation\AdminAreaBundle\Entity\ShippingLabel $shippingLabels
     */
    public function removeShippingLabel(\Corporation\AdminAreaBundle\Entity\ShippingLabel $shippingLabels)
    {
        $this->shippingLabels->removeElement($shippingLabels);
    }

    /**
     * Get shippingLabels
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShippingLabels()
    {
        return $this->shippingLabels;
    }

    /**
     * Set customer
     *
     * @param \Corporation\AdminAreaBundle\Entity\Customer $customer
     * @return Shipping
     */
    public function setCustomer(\Corporation\AdminAreaBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Corporation\AdminAreaBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    public function setShippingLabelsQuantity($shippingLabelsQuantity)
    {
        $this->shippingLabelsQuantity = $shippingLabelsQuantity;

        return $this;
    }

    public function getShippingLabelsQuantity()
    {
        return $this->getShippingLabels()->count();
    }

    /**
     * Set charges
     *
     * @param string $charges
     * @return Shipping
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;

        return $this;
    }

    /**
     * Get charges
     *
     * @return string 
     */
    public function getCharges()
    {
        return $this->charges;
    }
}
