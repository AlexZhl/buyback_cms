<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity
 */
class Order
{
    const ORDER_STATUS_OPEN = 1;
    const ORDER_STATUS_COMPLETED = 2;
    const ORDER_STATUS_CANCELLED = 3;

    const DELIVERY_METHOD_USPS_DOMESTIC_EXPRESS = 1;

    const DELIVERY_STATUS_OPEN = 1;         // Not processed yet
    const DELIVERY_STATUS_PENDING = 2;      // Packing
    const DELIVERY_STATUS_SENT = 3;         // In transit
    const DELIVERY_STATUS_CANCELLED = 4;    // Delivery cancelled
    const DELIVERY_STATUS_COMPLETED = 5;    // Delivered
    const DELIVERY_STATUS_RETURNED = 6;     // Returned to sender

    const PAYMENT_METHOD_AUTHORIZE_NET = 0;
    const PAYMENT_METHOD_PAYPAL = 1;

    const PAYMENT_STATUS_ORDER_UNKNOWN = -1;    // the order is unknown
    const PAYMENT_STATUS_OPEN = 0;              // created but not validated
    const PAYMENT_STATUS_PENDING = 1;           // the bank send a 'pending-like' status, so the payment is not validated, but the user payed
    const PAYMENT_STATUS_VALIDATED = 2;         // the bank confirm the payment
    const PAYMENT_STATUS_CANCELLED = 3;         // the user cancelled the payment
    const PAYMENT_STATUS_UNKNOWN = 4;           // the bank sent a unknown code ...
    const PAYMENT_STATUS_ERROR_VALIDATION = 9;  // something wrong happen when the bank validate the postback
    const PAYMENT_STATUS_WRONG_CALLBACK = 10;   // something wrong is sent from the bank. hack or the bank change something ...
    const PAYMENT_STATUS_WRONG_REQUEST = 11;    // the callback request is not valid
    const PAYMENT_STATUS_ORDER_NOT_OPEN = 12;   // the order is not open (so a previous transaction already alter the order)

    const PAYMENT_PAYPAL_STATUS_CANCELED_REVERSAL = 'Canceled_Reversal';
    const PAYMENT_PAYPAL_STATUS_COMPLETED = 'Completed';
    const PAYMENT_PAYPAL_STATUS_DENIED = 'Denied';
    const PAYMENT_PAYPAL_STATUS_FAILED = 'Failed';
    const PAYMENT_PAYPAL_STATUS_PENDING = 'Pending';
    const PAYMENT_PAYPAL_STATUS_REFUNDED = 'Refunded';
    const PAYMENT_PAYPAL_STATUS_REVERSED = 'Reversed';
    const PAYMENT_PAYPAL_STATUS_PROCESSED = 'Processed';
    const PAYMENT_PAYPAL_STATUS_VOIDED = 'Voided';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_method", type="integer")
     */
    private $paymentMethod;

    /**
     * @var integer
     *
     * @ORM\Column(name="transaction_number", type="string")
     */
    private $transactionNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="deliveryMethod", type="integer")
     */
    private $deliveryMethod;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_status", type="integer")
     */
    private $deliveryStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_status", type="integer")
     */
    private $paymentStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_firstname", type="string")
     */
    private $shippingFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_lastname", type="string")
     */
    private $shippingLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_line_1", type="string")
     */
    private $shippingAddressLine1;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_line_2", type="string")
     */
    private $shippingAddressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_city", type="string")
     */
    private $shippingCity;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_region", type="string")
     */
    private $shippingRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_postal_code", type="string")
     */
    private $shippingPostalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_country", type="string")
     */
    private $shippingCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_email", type="string")
     */
    private $customerEmail;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="orders")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity="OrderElement", mappedBy="order")
     */
    private $orderElements;

    public function __construct(Customer $customer)
    {
        $this->createdAt = new \DateTime();

        $this->setCustomerEmail($customer->getEmail());
        $this->setPhone($customer->getPhone());
        $this->setShippingFirstname($customer->getFirstname());
        $this->setShippingLastname($customer->getLastname());
        $this->setShippingAddressLine1($customer->getAddressLine1());
        $this->setShippingAddressLine2($customer->getAddressLine2());
        $this->setShippingCity($customer->getCity());
        $this->setShippingRegion($customer->getRegion());
        $this->setShippingPostalCode($customer->getPostalCode());
        $this->setShippingCountry($customer->getCountry());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     * @return Order
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return integer 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set deliveryMethod
     *
     * @param integer $deliveryMethod
     * @return Order
     */
    public function setDeliveryMethod($deliveryMethod)
    {
        $this->deliveryMethod = $deliveryMethod;

        return $this;
    }

    /**
     * Get deliveryMethod
     *
     * @return integer 
     */
    public function getDeliveryMethod()
    {
        return $this->deliveryMethod;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set deliveryStatus
     *
     * @param integer $deliveryStatus
     * @return Order
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return integer 
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * Set paymentStatus
     *
     * @param integer $paymentStatus
     * @return Order
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return integer 
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set shippingAddress1
     *
     * @param string $shippingAddress1
     * @return Order
     */
    public function setShippingAddress1($shippingAddress1)
    {
        $this->shippingAddress1 = $shippingAddress1;

        return $this;
    }

    /**
     * Get shippingAddress1
     *
     * @return string 
     */
    public function getShippingAddress1()
    {
        return $this->shippingAddress1;
    }

    /**
     * Set shippingAddress2
     *
     * @param string $shippingAddress2
     * @return Order
     */
    public function setShippingAddress2($shippingAddress2)
    {
        $this->shippingAddress2 = $shippingAddress2;

        return $this;
    }

    /**
     * Get shippingAddress2
     *
     * @return string 
     */
    public function getShippingAddress2()
    {
        return $this->shippingAddress2;
    }

    /**
     * Set shippingCity
     *
     * @param string $shippingCity
     * @return Order
     */
    public function setShippingCity($shippingCity)
    {
        $this->shippingCity = $shippingCity;

        return $this;
    }

    /**
     * Get shippingCity
     *
     * @return string 
     */
    public function getShippingCity()
    {
        return $this->shippingCity;
    }

    /**
     * Set shippingCountry
     *
     * @param string $shippingCountry
     * @return Order
     */
    public function setShippingCountry($shippingCountry)
    {
        $this->shippingCountry = $shippingCountry;

        return $this;
    }

    /**
     * Get shippingCountry
     *
     * @return string 
     */
    public function getShippingCountry()
    {
        return $this->shippingCountry;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add orderElements
     *
     * @param \Corporation\AdminAreaBundle\Entity\OrderElement $orderElements
     * @return Order
     */
    public function addOrderElement(\Corporation\AdminAreaBundle\Entity\OrderElement $orderElements)
    {
        $this->orderElements[] = $orderElements;

        return $this;
    }

    /**
     * Remove orderElements
     *
     * @param \Corporation\AdminAreaBundle\Entity\OrderElement $orderElements
     */
    public function removeOrderElement(\Corporation\AdminAreaBundle\Entity\OrderElement $orderElements)
    {
        $this->orderElements->removeElement($orderElements);
    }

    /**
     * Get orderElements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderElements()
    {
        return $this->orderElements;
    }

    /**
     * Set customer
     *
     * @param \Corporation\AdminAreaBundle\Entity\Customer $customer
     * @return Order
     */
    public function setCustomer(\Corporation\AdminAreaBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Corporation\AdminAreaBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set transactionNumber
     *
     * @param integer $transactionNumber
     * @return Order
     */
    public function setTransactionNumber($transactionNumber)
    {
        $this->transactionNumber = $transactionNumber;

        return $this;
    }

    /**
     * Get transactionNumber
     *
     * @return integer 
     */
    public function getTransactionNumber()
    {
        return $this->transactionNumber;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set customerEmail
     *
     * @param string $customerEmail
     * @return Order
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    /**
     * Get customerEmail
     *
     * @return string 
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * Set shippingFirstname
     *
     * @param string $shippingFirstname
     * @return Order
     */
    public function setShippingFirstname($shippingFirstname)
    {
        $this->shippingFirstname = $shippingFirstname;

        return $this;
    }

    /**
     * Get shippingFirstname
     *
     * @return string 
     */
    public function getShippingFirstname()
    {
        return $this->shippingFirstname;
    }

    /**
     * Set shippingLastname
     *
     * @param string $shippingLastname
     * @return Order
     */
    public function setShippingLastname($shippingLastname)
    {
        $this->shippingLastname = $shippingLastname;

        return $this;
    }

    /**
     * Get shippingLastname
     *
     * @return string 
     */
    public function getShippingLastname()
    {
        return $this->shippingLastname;
    }

    /**
     * Set shippingPostalCode
     *
     * @param string $shippingPostalCode
     * @return Order
     */
    public function setShippingPostalCode($shippingPostalCode)
    {
        $this->shippingPostalCode = $shippingPostalCode;

        return $this;
    }

    /**
     * Get shippingPostalCode
     *
     * @return string 
     */
    public function getShippingPostalCode()
    {
        return $this->shippingPostalCode;
    }

    /**
     * Set shippingRegion
     *
     * @param string $shippingRegion
     * @return Order
     */
    public function setShippingRegion($shippingRegion)
    {
        $this->shippingRegion = $shippingRegion;

        return $this;
    }

    /**
     * Get shippingRegion
     *
     * @return string 
     */
    public function getShippingRegion()
    {
        return $this->shippingRegion;
    }

    /**
     * Set shippingAddressLine1
     *
     * @param string $shippingAddressLine1
     * @return Order
     */
    public function setShippingAddressLine1($shippingAddressLine1)
    {
        $this->shippingAddressLine1 = $shippingAddressLine1;

        return $this;
    }

    /**
     * Get shippingAddressLine1
     *
     * @return string 
     */
    public function getShippingAddressLine1()
    {
        return $this->shippingAddressLine1;
    }

    /**
     * Set shippingAddressLine2
     *
     * @param string $shippingAddressLine2
     * @return Order
     */
    public function setShippingAddressLine2($shippingAddressLine2)
    {
        $this->shippingAddressLine2 = $shippingAddressLine2;

        return $this;
    }

    /**
     * Get shippingAddressLine2
     *
     * @return string 
     */
    public function getShippingAddressLine2()
    {
        return $this->shippingAddressLine2;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Order
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    public static function getStatusList()
    {
        return [
            self::ORDER_STATUS_OPEN      => 'order_status_open',
            self::ORDER_STATUS_COMPLETED => 'order_status_completed',
            self::ORDER_STATUS_CANCELLED => 'order_status_cancelled',
        ];
    }

    public static function getDeliveryMethodList()
    {
        return [
            self::DELIVERY_METHOD_USPS_DOMESTIC_EXPRESS => 'delivery_method_usps_domestic_express',
        ];
    }

    public static function getDeliveryStatusList()
    {
        return [
            self::DELIVERY_STATUS_OPEN      => 'delivery_status_open',
            self::DELIVERY_STATUS_PENDING   => 'delivery_status_pending',
            self::DELIVERY_STATUS_SENT      => 'delivery_status_sent',
            self::DELIVERY_STATUS_CANCELLED => 'delivery_status_cancelled',
            self::DELIVERY_STATUS_COMPLETED => 'delivery_status_completed',
            self::DELIVERY_STATUS_RETURNED  => 'delivery_status_returned',
        ];
    }

    public static function getPaymentMethodList()
    {
        return [
            self::PAYMENT_METHOD_AUTHORIZE_NET => 'payment_method_authorize_net',
            self::PAYMENT_METHOD_PAYPAL        => 'payment_method_paypal',
        ];
    }

    public static function getPaymentStatusList()
    {
        return [
            self::PAYMENT_STATUS_ORDER_UNKNOWN    => 'payment_status_unknown',
            self::PAYMENT_STATUS_OPEN             => 'payment_status_open',
            self::PAYMENT_STATUS_PENDING          => 'payment_status_pending',
            self::PAYMENT_STATUS_VALIDATED        => 'payment_status_validated',
            self::PAYMENT_STATUS_CANCELLED        => 'payment_status_cancelled',
            self::PAYMENT_STATUS_UNKNOWN          => 'payment_status_unknown',
            self::PAYMENT_STATUS_ERROR_VALIDATION => 'payment_status_error_validation',
            self::PAYMENT_STATUS_WRONG_CALLBACK   => 'payment_status_wrong_callback',
            self::PAYMENT_STATUS_WRONG_REQUEST    => 'payment_status_wrong_request',
            self::PAYMENT_STATUS_ORDER_NOT_OPEN   => 'payment_status_order_not_open',
        ];
    }
}
