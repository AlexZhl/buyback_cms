<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="adminarea_customers")
 * @ORM\Entity()
 * @UniqueEntity(fields="email", message="Email already taken", groups={ "registration", "CustomerAdmin" })
 */
class Customer implements AdvancedUserInterface, \Serializable
{
    const RESET_PASSWORD_TOKEN_VALID_TIME = 86400; // 24hrs in sec

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @Assert\NotBlank(groups={"registration", "changePassword"})
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @Assert\NotBlank(groups={"registration", "CustomerAdmin"})
     * @Assert\Length(max=255, groups={"registration", "CustomerAdmin"})
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="trusted", type="boolean")
     */
    private $trusted;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address_line_1", type="string", length=255, nullable=true)
     */
    private $addressLine1;

    /**
     * @var string
     *
     * @ORM\Column(name="address_line_2", type="string", length=255, nullable=true)
     */
    private $addressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="reset_password_token", type="string", length=255, nullable=true, unique=true)
     */
    private $resetPasswordToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reset_password_requested_at", type="datetime", nullable=true)
     */
    private $resetPasswordRequestedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="customer")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="Shipping", mappedBy="customer")
     */
    private $shippings;

    /**
     * @var string
     */
    private $cardNumber;

    /**
     * @var string
     */
    private $cardCVV;

    /**
     * @var string
     */
    private $cardYear;

    /**
     * @var string
     */
    private $cardMonth;

    /**
     * @var boolean
     */
    private $billingSameAsShippingAddress = false;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_firstname", type="string", length=255, nullable=true)
     */
    private $billingFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_lastname", type="string", length=255, nullable=true)
     */
    private $billingLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_line_1", type="string", length=255, nullable=true)
     */
    private $billingAddressLine1;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_line_2", type="string", length=255, nullable=true)
     */
    private $billingAddressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_country", type="string", length=255, nullable=true)
     */
    private $billingCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_region", type="string", length=255, nullable=true)
     */
    private $billingRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_city", type="string", length=255, nullable=true)
     */
    private $billingCity;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_postal_code", type="string", length=255, nullable=true)
     */
    private $billingPostalCode;

    public function __construct()
    {
        $this->isActive = true;
        $this->createdAt = new \DateTime();
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function getRoles()
    {
        return ['ROLE_CUSTOMER'];
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->firstname,
            $this->lastname,
            $this->addressLine1,
            $this->addressLine2,
            $this->city,
            $this->region,
            $this->postalCode,
            $this->country,
            $this->phone,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->firstname,
            $this->lastname,
            $this->addressLine1,
            $this->addressLine2,
            $this->city,
            $this->region,
            $this->postalCode,
            $this->country,
            $this->phone,
            ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    public function isResetPasswordTokenValid()
    {
        if (time() < $this->resetPasswordRequestedAt->getTimestamp() + self::RESET_PASSWORD_TOKEN_VALID_TIME) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Customer
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set plainPassword
     *
     * @param string $plainPassword
     * @return Customer
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Customer
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Customer
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Customer
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return Customer
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set addressLine1
     *
     * @param string $addressLine1
     * @return Customer
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    /**
     * Get addressLine1
     *
     * @return string 
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * Set addressLine2
     *
     * @param string $addressLine2
     * @return Customer
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    /**
     * Get addressLine2
     *
     * @return string 
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Customer
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Customer
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Customer
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add orders
     *
     * @param \Corporation\AdminAreaBundle\Entity\Order $orders
     * @return Customer
     */
    public function addOrder(\Corporation\AdminAreaBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \Corporation\AdminAreaBundle\Entity\Order $orders
     */
    public function removeOrder(\Corporation\AdminAreaBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set resetPasswordToken
     *
     * @param string $resetPasswordToken
     * @return Customer
     */
    public function setResetPasswordToken($resetPasswordToken)
    {
        $this->resetPasswordToken = $resetPasswordToken;
        $this->resetPasswordRequestedAt = new \DateTime();

        return $this;
    }

    /**
     * Get resetPasswordToken
     *
     * @return string 
     */
    public function getResetPasswordToken()
    {
        return $this->resetPasswordToken;
    }

    /**
     * Set resetPasswordRequestedAt
     *
     * @param \DateTime $resetPasswordRequestedAt
     * @return Customer
     */
    public function setResetPasswordRequestedAt($resetPasswordRequestedAt)
    {
        $this->resetPasswordRequestedAt = $resetPasswordRequestedAt;

        return $this;
    }

    /**
     * Get resetPasswordRequestedAt
     *
     * @return \DateTime 
     */
    public function getResetPasswordRequestedAt()
    {
        return $this->resetPasswordRequestedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Customer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set billingFirstname
     *
     * @param string $billingFirstname
     * @return Customer
     */
    public function setBillingFirstname($billingFirstname)
    {
        $this->billingFirstname = $billingFirstname;

        return $this;
    }

    /**
     * Get billingFirstname
     *
     * @return string 
     */
    public function getBillingFirstname()
    {
        return $this->billingFirstname;
    }

    /**
     * Set billingLastname
     *
     * @param string $billingLastname
     * @return Customer
     */
    public function setBillingLastname($billingLastname)
    {
        $this->billingLastname = $billingLastname;

        return $this;
    }

    /**
     * Get billingLastname
     *
     * @return string 
     */
    public function getBillingLastname()
    {
        return $this->billingLastname;
    }

    /**
     * Set billingAddressLine1
     *
     * @param string $billingAddressLine1
     * @return Customer
     */
    public function setBillingAddressLine1($billingAddressLine1)
    {
        $this->billingAddressLine1 = $billingAddressLine1;

        return $this;
    }

    /**
     * Get billingAddressLine1
     *
     * @return string 
     */
    public function getBillingAddressLine1()
    {
        return $this->billingAddressLine1;
    }

    /**
     * Set billingAddressLine2
     *
     * @param string $billingAddressLine2
     * @return Customer
     */
    public function setBillingAddressLine2($billingAddressLine2)
    {
        $this->billingAddressLine2 = $billingAddressLine2;

        return $this;
    }

    /**
     * Get billingAddressLine2
     *
     * @return string 
     */
    public function getBillingAddressLine2()
    {
        return $this->billingAddressLine2;
    }

    /**
     * Set billingCountry
     *
     * @param string $billingCountry
     * @return Customer
     */
    public function setBillingCountry($billingCountry)
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }

    /**
     * Get billingCountry
     *
     * @return string 
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * Set billingRegion
     *
     * @param string $billingRegion
     * @return Customer
     */
    public function setBillingRegion($billingRegion)
    {
        $this->billingRegion = $billingRegion;

        return $this;
    }

    /**
     * Get billingRegion
     *
     * @return string 
     */
    public function getBillingRegion()
    {
        return $this->billingRegion;
    }

    /**
     * Set billingCity
     *
     * @param string $billingCity
     * @return Customer
     */
    public function setBillingCity($billingCity)
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    /**
     * Get billingCity
     *
     * @return string 
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * Set billingPostalCode
     *
     * @param string $billingPostalCode
     * @return Customer
     */
    public function setBillingPostalCode($billingPostalCode)
    {
        $this->billingPostalCode = $billingPostalCode;

        return $this;
    }

    /**
     * Get billingPostalCode
     *
     * @return string 
     */
    public function getBillingPostalCode()
    {
        return $this->billingPostalCode;
    }

    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     * @return Customer
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string 
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set cardCVV
     *
     * @param string $cardCVV
     * @return Customer
     */
    public function setCardCVV($cardCVV)
    {
        $this->cardCVV = $cardCVV;

        return $this;
    }

    /**
     * Get cardCVV
     *
     * @return string 
     */
    public function getCardCVV()
    {
        return $this->cardCVV;
    }

    /**
     * Set cardYear
     *
     * @param string $cardYear
     * @return Customer
     */
    public function setCardYear($cardYear)
    {
        $this->cardYear = $cardYear;

        return $this;
    }

    /**
     * Get cardYear
     *
     * @return string 
     */
    public function getCardYear()
    {
        return $this->cardYear;
    }

    /**
     * Set cardMonth
     *
     * @param string $cardMonth
     * @return Customer
     */
    public function setCardMonth($cardMonth)
    {
        $this->cardMonth = $cardMonth;

        return $this;
    }

    /**
     * Get cardMonth
     *
     * @return string 
     */
    public function getCardMonth()
    {
        return $this->cardMonth;
    }

    /**
     * Set billingSameAsShippingAddress
     *
     * @param boolean $billingSameAsShippingAddress
     * @return Customer
     */
    public function setBillingSameAsShippingAddress($billingSameAsShippingAddress)
    {
        $this->billingSameAsShippingAddress = $billingSameAsShippingAddress;

        return $this;
    }

    /**
     * Get billingSameAsShippingAddress
     *
     * @return boolean
     */
    public function getBillingSameAsShippingAddress()
    {
        return $this->billingSameAsShippingAddress;
    }

    public function getName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function copyShippingToBilling()
    {
        $this->billingFirstname = $this->firstname;
        $this->billingLastname = $this->lastname;
        $this->billingAddressLine1 = $this->addressLine1;
        $this->billingAddressLine2 = $this->addressLine2;
        $this->billingCity = $this->city;
        $this->billingRegion = $this->region;
        $this->billingPostalCode = $this->postalCode;
        $this->billingCountry = $this->country;
    }

    public function clearCardData()
    {
        $this->cardNumber = null;
        $this->cardCVV = null;
        $this->cardYear = null;
        $this->cardMonth = null;
    }

    /**
     * Add shipping
     *
     * @param \Corporation\AdminAreaBundle\Entity\Shipping $shipping
     * @return Customer
     */
    public function addShipping(\Corporation\AdminAreaBundle\Entity\Shipping $shipping)
    {
        $this->shipping[] = $shipping;

        return $this;
    }

    /**
     * Remove shipping
     *
     * @param \Corporation\AdminAreaBundle\Entity\Shipping $shipping
     */
    public function removeShipping(\Corporation\AdminAreaBundle\Entity\Shipping $shipping)
    {
        $this->shipping->removeElement($shipping);
    }

    /**
     * Get shipping
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set trusted
     *
     * @param boolean $trusted
     * @return Customer
     */
    public function setTrusted($trusted)
    {
        $this->trusted = $trusted;

        return $this;
    }

    /**
     * Get trusted
     *
     * @return boolean 
     */
    public function getTrusted()
    {
        return $this->trusted;
    }
}
