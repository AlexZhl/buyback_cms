<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;

/**
 * @ORM\Table(name="places")
 * @ORM\Entity
 * @ORM\EntityListeners({"Corporation\AdminAreaBundle\EventListener\PlaceListener"})
 */
class Place
{
    const PLACE_TYPE_SELLING = 1;
    const PLACE_TYPE_DOESNT_SELL = 2;
    const PLACE_TYPE_CONFUSED = 3;
    const PLACE_TYPE_WANT_TO_GO = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="decimal", scale=6)
     * )
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="decimal", scale=6)
     */
    private $lng;

    /**
     * @ORM\Column(name="new_screens_each_day", type="integer")
     */
    private $newScreensEachDay;

    /**
     * @ORM\Column(name="last_visit", type="datetime", nullable=true)
     */
    private $lastVisit;

    /**
     * @ORM\Column(name="last_purchase_total", type="decimal", precision=8, scale=2, nullable=true)
     */
    private $lastPurchaseTotal;

    /**
     * @ORM\Column(name="last_purchase_screens", type="integer", nullable=true)
     */
    private $lastPurchaseScreens;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="last_purchaser_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $lastPurchaser;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $manager;

    /**
     * @ORM\Column(name="next_visit", type="datetime", nullable=true)
     */
    private $nextVisit;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \Corporation\AdminAreaBundle\Entity\Region
     *
     * @ORM\ManyToOne(targetEntity="\Corporation\AdminAreaBundle\Entity\Region", cascade={"persist"})
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $region;

    /**
     * @var \Corporation\AdminAreaBundle\Entity\Customer
     *
     * @ORM\ManyToOne(targetEntity="\Corporation\AdminAreaBundle\Entity\Customer", cascade={"persist"})
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $contactPerson;

    /**
     * @ORM\OneToMany(targetEntity="Purchase", mappedBy="place")
     */
    private $purchases;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = false;

    // Trick for command corporation:places:import
    protected $avoidGeocoderRequest;

    public function __construct()
    {
        $this->avoidGeocoderRequest = false;
    }

    public function getExpectedScreens()
    {
        $expectedScreens = 0;
        if ($this->lastVisit) {
            $expectedScreens = intval((new \DateTime())->diff($this->lastVisit)->days * $this->newScreensEachDay);
            if ($expectedScreens < 0) {
                $expectedScreens = 0;
            }
        }
        return $expectedScreens;
    }

    public function updateLastVisit()
    {
        $lastVisit = null;
        $lastPurchaseTotal = 0;
        $lastPurchaseScreens = 0;
        $lastPurchaser = null;
        foreach ($this->purchases as $purchase) {
            if ($lastVisit == null) {
                $lastVisit = $purchase->getCreatedAt();
                $lastPurchaseTotal = $purchase->getTotal();
                $lastPurchaseScreens = $purchase->getTotalScreens();
                $lastPurchaser = $purchase->getPurchaser();
            } elseif ($purchase->getCreatedAt() >= $lastVisit) {
                $lastVisit = $purchase->getCreatedAt();
                $lastPurchaseTotal = $purchase->getTotal();
                $lastPurchaseScreens = $purchase->getTotalScreens();
                $lastPurchaser = $purchase->getPurchaser();
            }
        }
        $this->setLastVisit($lastVisit);
        $this->setLastPurchaseTotal($lastPurchaseTotal);
        $this->setLastPurchaseScreens($lastPurchaseScreens);
        $this->setLastPurchaser($lastPurchaser);
    }

    public function getLastPurchaserNameDyn()
    {
        return $this->getLastPurchaser() ? $this->getLastPurchaser()->getFullname() : '';
    }

    public function getManagerNameDyn()
    {
        return $this->getManager() ? $this->getManager()->getFullname() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Place
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Place
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Place
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     * @return Place
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Place
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set contactPerson
     *
     * @param \Corporation\AdminAreaBundle\Entity\Customer $contactPerson
     * @return Place
     */
    public function setContactPerson(\Corporation\AdminAreaBundle\Entity\Customer $contactPerson = null)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return \Corporation\AdminAreaBundle\Entity\Customer
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set region
     *
     * @param \Corporation\AdminAreaBundle\Entity\Region $region
     * @return Place
     */
    public function setRegion(\Corporation\AdminAreaBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Corporation\AdminAreaBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set newScreensEachDay
     *
     * @param integer $newScreensEachDay
     * @return Place
     */
    public function setNewScreensEachDay($newScreensEachDay)
    {
        $this->newScreensEachDay = $newScreensEachDay;

        return $this;
    }

    /**
     * Get newScreensEachDay
     *
     * @return integer
     */
    public function getNewScreensEachDay()
    {
        return $this->newScreensEachDay;
    }

    public function getLastVisit()
    {
        return $this->lastVisit;
    }

    /**
     * Set lastVisit
     *
     * @param \DateTime $lastVisit
     * @return Place
     */
    public function setLastVisit($lastVisit)
    {
        $this->lastVisit = $lastVisit;

        return $this;
    }

    /**
     * Add purchases
     *
     * @param \Corporation\AdminAreaBundle\Entity\Purchase $purchases
     * @return Place
     */
    public function addPurchase(\Corporation\AdminAreaBundle\Entity\Purchase $purchases)
    {
        $this->purchases[] = $purchases;

        return $this;
    }

    /**
     * Remove purchases
     *
     * @param \Corporation\AdminAreaBundle\Entity\Purchase $purchases
     */
    public function removePurchase(\Corporation\AdminAreaBundle\Entity\Purchase $purchases)
    {
        $this->purchases->removeElement($purchases);
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Place
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    public function setLatLng($latlng)
    {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);
        return $this;
    }

    /**
     * @Assert\NotBlank()
     * @OhAssert\LatLng()
     */
    public function getLatLng()
    {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Place
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    public function getTypeName()
    {
        $typesList = self::getPlaceTypesList();
        return $typesList[$this->getType()];
    }

    public static function getPlaceTypesList()
    {
        return [
            self::PLACE_TYPE_SELLING => 'place_type_selling',
            self::PLACE_TYPE_DOESNT_SELL => 'place_type_doesnt_sell',
            self::PLACE_TYPE_CONFUSED => 'place_type_confused',
            self::PLACE_TYPE_WANT_TO_GO => 'place_type_want_to_go',
        ];
    }

    /**
     * Set lastPurchaseTotal
     *
     * @param string $lastPurchaseTotal
     * @return Place
     */
    public function setLastPurchaseTotal($lastPurchaseTotal)
    {
        $this->lastPurchaseTotal = $lastPurchaseTotal;

        return $this;
    }

    /**
     * Get lastPurchaseTotal
     *
     * @return string 
     */
    public function getLastPurchaseTotal()
    {
        return $this->lastPurchaseTotal;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Place
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set lastPurchaseScreens
     *
     * @param integer $lastPurchaseScreens
     * @return Place
     */
    public function setLastPurchaseScreens($lastPurchaseScreens)
    {
        $this->lastPurchaseScreens = $lastPurchaseScreens;

        return $this;
    }

    /**
     * Get lastPurchaseScreens
     *
     * @return integer 
     */
    public function getLastPurchaseScreens()
    {
        return $this->lastPurchaseScreens;
    }

    /**
     * Set lastPurchaser
     *
     * @param \Application\Sonata\UserBundle\Entity\User $lastPurchaser
     * @return Place
     */
    public function setLastPurchaser(\Application\Sonata\UserBundle\Entity\User $lastPurchaser = null)
    {
        $this->lastPurchaser = $lastPurchaser;

        return $this;
    }

    /**
     * Get lastPurchaser
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getLastPurchaser()
    {
        return $this->lastPurchaser;
    }

    public function __toString()
    {
        return $this->getName() ? $this->getName() : ' ';
    }

    public function getAvoidGeocoderRequest()
    {
        return $this->avoidGeocoderRequest;
    }

    public function setAvoidGeocoderRequest($avoidGeocoderRequest)
    {
        $this->avoidGeocoderRequest = $avoidGeocoderRequest;

        return $this;
    }

    /**
     * Set manager
     *
     * @param \Application\Sonata\UserBundle\Entity\User $manager
     * @return Place
     */
    public function setManager(\Application\Sonata\UserBundle\Entity\User $manager = null)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set nextVisit
     *
     * @param \DateTime $nextVisit
     * @return Place
     */
    public function setNextVisit($nextVisit)
    {
        $this->nextVisit = $nextVisit;

        return $this;
    }

    /**
     * Get nextVisit
     *
     * @return \DateTime 
     */
    public function getNextVisit()
    {
        return $this->nextVisit;
    }
}
