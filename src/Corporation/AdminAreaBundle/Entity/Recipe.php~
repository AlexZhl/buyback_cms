<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="recipes")
 */
class Recipe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", nullable=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="recipe_steps", type="json", nullable=true)
     */
    protected $recipeSteps;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="video_media", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $video;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="recipes")
     * @ORM\JoinTable(
     *     name="recipes_to_products",
     *     joinColumns={
     *      @ORM\JoinColumn(name="recipe_id", referencedColumnName="id")
     *  },
     *     inverseJoinColumns={
     *      @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *  })
     */
    protected $products;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Recipe
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Recipe
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Recipe
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Add product
     *
     * @param Product $product
     * @return Recipe
     */
    public function addProduct(Product $product)
    {
        if ($this->products->contains($product)) {
            return $this;
        }
        $this->products->add($product);
        $product->addRecipe($this);

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     * @return Recipe
     */
    public function removeProduct(Product $product)
    {
        if (!$this->products->contains($product)) {
            return $this;
        }
        $this->products->removeElement($product);
        $product->removeRecipe($this);

        return $this;
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set recipeSteps
     *
     * @param json $recipeSteps
     * @return Recipe
     */
    public function setRecipeSteps($recipeSteps)
    {
        $this->recipeSteps = $recipeSteps;

        return $this;
    }

    /**
     * Get recipeSteps
     *
     * @return json 
     */
    public function getRecipeSteps()
    {
        return $this->recipeSteps;
    }
}
