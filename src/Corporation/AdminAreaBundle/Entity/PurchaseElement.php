<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="purchase_elements")
 * @ORM\Entity
 */
class PurchaseElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Corporation\AdminAreaBundle\Entity\Purchase", inversedBy="purchaseElements", cascade={"persist"})
     * @ORM\JoinColumn(name="purchase_id", referencedColumnName="id")
     */
    private $purchase;

    /**
     * @ORM\ManyToOne(targetEntity="Corporation\AdminAreaBundle\Entity\ScreenModel")
     * @ORM\JoinColumn(name="screen_model_id", referencedColumnName="id")
     */
    private $screenModel;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity_oem", type="integer", nullable=true)
     */
    private $quantityOem;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity_issues", type="integer", nullable=true)
     */
    private $quantityIssues;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity_bad", type="integer", nullable=true)
     */
    private $quantityBad;

    /**
     * @var string
     *
     * @ORM\Column(name="price_oem", type="decimal", scale=2, nullable=true)
     */
    private $priceOem;

    /**
     * @var string
     *
     * @ORM\Column(name="price_issues", type="decimal", scale=2, nullable=true)
     */
    private $priceIssues;

    /**
     * @var string
     *
     * @ORM\Column(name="price_bad", type="decimal", scale=2, nullable=true)
     */
    private $priceBad;

    public function __toString()
    {
        if ($this->getId()) {
            return sprintf('%s | OEM %s pcs | Issues %s pcs | Bad %s pcs | Total %s$',
                $this->getScreenModel()->getName(),
                $this->getQuantityOem() ? $this->getQuantityOem() : 0,
                $this->getQuantityIssues() ? $this->getQuantityIssues() : 0,
                $this->getQuantityBad() ? $this->getQuantityBad() : 0,
                $this->getTotalWithBad()
            );
        } else {
            return 'New Purchase Element';
        }
    }

    public function getTotal()
    {
        return $this->getPriceOem() * $this->getQuantityOem() + $this->getPriceIssues() * $this->getQuantityIssues();
    }

    public function getTotalScreens()
    {
        return $this->getQuantityOem() + $this->getQuantityIssues();
    }

    public function getTotalWithBad()
    {
        return $this->getTotal() + $this->getPriceBad() * $this->getQuantityBad();
    }

    public function getTotalScreensWithBad()
    {
        return $this->getTotalScreens() + $this->getQuantityBad();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantityOem
     *
     * @param integer $quantityOem
     * @return PurchaseElement
     */
    public function setQuantityOem($quantityOem)
    {
        $this->quantityOem = $quantityOem;

        return $this;
    }

    /**
     * Get quantityOem
     *
     * @return integer 
     */
    public function getQuantityOem()
    {
        return $this->quantityOem;
    }

    /**
     * Set quantityIssues
     *
     * @param integer $quantityIssues
     * @return PurchaseElement
     */
    public function setQuantityIssues($quantityIssues)
    {
        $this->quantityIssues = $quantityIssues;

        return $this;
    }

    /**
     * Get quantityIssues
     *
     * @return integer 
     */
    public function getQuantityIssues()
    {
        return $this->quantityIssues;
    }

    /**
     * Set quantityBad
     *
     * @param integer $quantityBad
     * @return PurchaseElement
     */
    public function setQuantityBad($quantityBad)
    {
        $this->quantityBad = $quantityBad;

        return $this;
    }

    /**
     * Get quantityBad
     *
     * @return integer 
     */
    public function getQuantityBad()
    {
        return $this->quantityBad;
    }

    /**
     * Set priceOem
     *
     * @param string $priceOem
     * @return PurchaseElement
     */
    public function setPriceOem($priceOem)
    {
        $this->priceOem = $priceOem;

        return $this;
    }

    /**
     * Get priceOem
     *
     * @return string 
     */
    public function getPriceOem()
    {
        return $this->priceOem;
    }

    /**
     * Set priceIssues
     *
     * @param string $priceIssues
     * @return PurchaseElement
     */
    public function setPriceIssues($priceIssues)
    {
        $this->priceIssues = $priceIssues;

        return $this;
    }

    /**
     * Get priceIssues
     *
     * @return string 
     */
    public function getPriceIssues()
    {
        return $this->priceIssues;
    }

    /**
     * Set priceBad
     *
     * @param string $priceBad
     * @return PurchaseElement
     */
    public function setPriceBad($priceBad)
    {
        $this->priceBad = $priceBad;

        return $this;
    }

    /**
     * Get priceBad
     *
     * @return string 
     */
    public function getPriceBad()
    {
        return $this->priceBad;
    }

    /**
     * Set purchase
     *
     * @param \Corporation\AdminAreaBundle\Entity\Purchase $purchase
     * @return PurchaseElement
     */
    public function setPurchase(\Corporation\AdminAreaBundle\Entity\Purchase $purchase = null)
    {
        $this->purchase = $purchase;

        return $this;
    }

    /**
     * Get purchase
     *
     * @return \Corporation\AdminAreaBundle\Entity\Purchase 
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * Set screenModel
     *
     * @param \Corporation\AdminAreaBundle\Entity\ScreenModel $screenModel
     * @return PurchaseElement
     */
    public function setScreenModel(\Corporation\AdminAreaBundle\Entity\ScreenModel $screenModel = null)
    {
        $this->screenModel = $screenModel;

        return $this;
    }

    /**
     * Get screenModel
     *
     * @return \Corporation\AdminAreaBundle\Entity\ScreenModel 
     */
    public function getScreenModel()
    {
        return $this->screenModel;
    }
}
