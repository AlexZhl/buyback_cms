<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="screen_models")
 * @ORM\Entity
 */
class ScreenModel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sys_name", type="string", length=255, unique=true))
     */
    private $sysName;

    /**
     * @var string
     *
     * @ORM\Column(name="default_oem_price", type="decimal", scale=2, nullable=true)
     */
    private $defaultOemPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="default_issues_price", type="decimal", scale=2, nullable=true)
     */
    private $defaultIssuesPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="default_bad_price", type="decimal", scale=2, nullable=true)
     */
    private $defaultBadPrice;

    public function __construct()
    {
    }

    public function __toString()
    {
        return $this->getName() ? $this->getName() : ' ';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->setSysName($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sysName
     *
     * @param string $sysName
     * @return ScreenModel
     */
    public function setSysName($sysName)
    {
        $this->sysName = Urlizer::urlize($sysName, '-');

        return $this;
    }

    /**
     * Get sysName
     *
     * @return string 
     */
    public function getSysName()
    {
        return $this->sysName;
    }

    /**
     * Set defaultOemPrice
     *
     * @param string $defaultOemPrice
     * @return ScreenModel
     */
    public function setDefaultOemPrice($defaultOemPrice)
    {
        $this->defaultOemPrice = $defaultOemPrice;

        return $this;
    }

    /**
     * Get defaultOemPrice
     *
     * @return string 
     */
    public function getDefaultOemPrice()
    {
        return $this->defaultOemPrice;
    }

    /**
     * Set defaultIssuesPrice
     *
     * @param string $defaultIssuesPrice
     * @return ScreenModel
     */
    public function setDefaultIssuesPrice($defaultIssuesPrice)
    {
        $this->defaultIssuesPrice = $defaultIssuesPrice;

        return $this;
    }

    /**
     * Get defaultIssuesPrice
     *
     * @return string 
     */
    public function getDefaultIssuesPrice()
    {
        return $this->defaultIssuesPrice;
    }

    /**
     * Set defaultBadPrice
     *
     * @param string $defaultBadPrice
     * @return ScreenModel
     */
    public function setDefaultBadPrice($defaultBadPrice)
    {
        $this->defaultBadPrice = $defaultBadPrice;

        return $this;
    }

    /**
     * Get defaultBadPrice
     *
     * @return string 
     */
    public function getDefaultBadPrice()
    {
        return $this->defaultBadPrice;
    }
}
