<?php

namespace Corporation\AdminAreaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="customer_shipping_labels")
 * @ORM\Entity()
 */
class ShippingLabel
{
    const RESET_PASSWORD_TOKEN_VALID_TIME = 86400; // 24hrs in sec

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Shipping", inversedBy="shippingLabels")
     * @ORM\JoinColumn(name="shipping_id", referencedColumnName="id")
     */
    private $shipping;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_number", type="string", length=255)
     */
    private $trackingNumber;

    /**
     * @ORM\Column(name="weight", type="decimal", scale=6)
     */
    private $weight;

    /**
     * @ORM\Column(name="length", type="decimal", scale=6)
     */
    private $length;

    /**
     * @ORM\Column(name="width", type="decimal", scale=6)
     */
    private $width;

    /**
     * @ORM\Column(name="height", type="decimal", scale=6)
     */
    private $height;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trackingNumber
     *
     * @param string $trackingNumber
     * @return ShippingLabel
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    /**
     * Get trackingNumber
     *
     * @return string 
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * Set shipping
     *
     * @param \Corporation\AdminAreaBundle\Entity\Shipping $shipping
     * @return ShippingLabel
     */
    public function setShipping(\Corporation\AdminAreaBundle\Entity\Shipping $shipping = null)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return \Corporation\AdminAreaBundle\Entity\Shipping 
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return ShippingLabel
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set length
     *
     * @param string $length
     * @return ShippingLabel
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string 
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set width
     *
     * @param string $width
     * @return ShippingLabel
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return ShippingLabel
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string 
     */
    public function getHeight()
    {
        return $this->height;
    }
}
