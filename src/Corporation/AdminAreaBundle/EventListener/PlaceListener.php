<?php

namespace Corporation\AdminAreaBundle\EventListener;

use Corporation\AdminAreaBundle\Entity\Place;
use Corporation\AdminAreaBundle\Entity\Region;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderAddressType;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderCoordinateRequest;
use Ivory\GoogleMap\Service\Geocoder\Response\GeocoderStatus;

class PlaceListener
{
    private $geocoder;

    public function __construct(GeocoderService $geocoder)
    {
        $this->geocoder = $geocoder;
    }

    public function prePersist(Place $place, LifecycleEventArgs $eventArgs)
    {
        $this->updateGeoData($place, $eventArgs->getEntityManager());
    }

    public function preUpdate(Place $place, PreUpdateEventArgs $eventArgs)
    {
        if ($eventArgs->hasChangedField('lat') || $eventArgs->hasChangedField('lng')) {
            $this->updateGeoData($place, $eventArgs->getEntityManager());
        }
    }

    public function updateGeoData(Place $place, EntityManager $entityManager)
    {
        if ($place->getAvoidGeocoderRequest()) {
            return;
        }

        $request = new GeocoderCoordinateRequest(new Coordinate($place->getLat(), $place->getLng()));
        $request->setResultTypes([GeocoderAddressType::STREET_ADDRESS, GeocoderAddressType::PREMISE, GeocoderAddressType::ADMINISTRATIVE_AREA_LEVEL_1]);
        $response = $this->geocoder->geocode($request);

        if ($response->getStatus() == GeocoderStatus::OK) {
            $responseResults = $response->getResults();

            $premiseFound = null;
            $streetFound = null;
            $regionFound = null;
            foreach ($response->getResults() as $result) {
                foreach ($result->getTypes() as $type) {
                    if ($type == GeocoderAddressType::PREMISE) {
                        $premiseFound = $result->getFormattedAddress();
                    } elseif ($type == GeocoderAddressType::STREET_ADDRESS) {
                        $streetFound = $result->getFormattedAddress();
                    } elseif ($type == GeocoderAddressType::ADMINISTRATIVE_AREA_LEVEL_1) {
                        $regionFound = $result->getFormattedAddress();
                    }
                }
            }

            if ($premiseFound) {
                $place->setAddress($premiseFound);
            } elseif ($streetFound) {
                $place->setAddress($streetFound);
            }
            if ($regionFound) {
                $region = $entityManager->getRepository(Region::class)->findOneBy(['name' => $regionFound]);
                if (!$region) {
                    $region = new Region();
                    $region->setName($regionFound);
                    $entityManager->persist($region);
                    $entityManager->flush($region);
                }
                $place->setRegion($region);
            }
        } else {
            throw new \Exception('Geocoder Error: ' . $response->getStatus());
        }
    }
}