<?php

namespace Corporation\AdminAreaBundle\EventListener;

use Application\Sonata\UserBundle\Entity\User;
use Corporation\AdminAreaBundle\Entity\Place;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Corporation\AdminAreaBundle\Entity\Purchase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CorporationSubscriber implements EventSubscriber
{
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof Purchase) {
            $currentUser = $this->tokenStorage->getToken()->getUser();
            if ($currentUser instanceof User) {
                $object->setPurchaser($currentUser);
            }
        }
    }
}