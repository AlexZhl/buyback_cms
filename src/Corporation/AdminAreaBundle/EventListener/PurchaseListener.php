<?php

namespace Corporation\AdminAreaBundle\EventListener;

use Corporation\AdminAreaBundle\Entity\Purchase;
use Corporation\AdminAreaBundle\Entity\PurchaseElement;
use Corporation\AdminAreaBundle\Entity\ScreenModel;
use Corporation\AdminAreaBundle\Manager\MailManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PurchaseListener
{
    protected $mailManager;

    public function setMailManager(MailManager $mailManager)
    {
        $this->mailManager = $mailManager;
    }

    public function prePersist(Purchase $purchase, LifecycleEventArgs $eventArgs)
    {
        // TODO: Create beautifull way to handle this
        $screenModelsPurchase = $purchase->getScreenModels();
        $screenModels = $eventArgs->getEntityManager()->getRepository(ScreenModel::class)->findAll();

        foreach ($screenModels as $screenModel) {
            $purchaseElement = new PurchaseElement();
            $purchaseElement->setScreenModel($screenModel);
            $purchaseElementNotEmpty = false;

            if ($screenModelsPurchase[$screenModel->getSysName() . '_oem'] && $screenModelsPurchase[$screenModel->getSysName() . '_oem_price']) {
                $purchaseElement->setQuantityOem($screenModelsPurchase[$screenModel->getSysName() . '_oem']);
                $purchaseElement->setPriceOem($screenModelsPurchase[$screenModel->getSysName() . '_oem_price']);
                $purchaseElementNotEmpty = true;
            }
            if ($screenModelsPurchase[$screenModel->getSysName() . '_issues'] && $screenModelsPurchase[$screenModel->getSysName() . '_issues_price']) {
                $purchaseElement->setQuantityIssues($screenModelsPurchase[$screenModel->getSysName() . '_issues']);
                $purchaseElement->setPriceIssues($screenModelsPurchase[$screenModel->getSysName() . '_issues_price']);
                $purchaseElementNotEmpty = true;
            }
            if ($screenModelsPurchase[$screenModel->getSysName() . '_bad'] && $screenModelsPurchase[$screenModel->getSysName() . '_bad_price']) {
                $purchaseElement->setQuantityBad($screenModelsPurchase[$screenModel->getSysName() . '_bad']);
                $purchaseElement->setPriceBad($screenModelsPurchase[$screenModel->getSysName() . '_bad_price']);
                $purchaseElementNotEmpty = true;
            }

            if ($purchaseElementNotEmpty) {
                $purchaseElement->setPurchase($purchase);
                $purchase->addPurchaseElement($purchaseElement);
            }
        }

        if ($screenModelsPurchase['custom_description_price']) {
            $purchase->setCustomAmount($screenModelsPurchase['custom_description_price']);
        }
        $purchase->setCustomAmountDescription($screenModelsPurchase['custom_description']);
    }

    public function postPersist(Purchase $purchase, LifecycleEventArgs $eventArgs)
    {
        $purchase->recalculateTotal();
        $eventArgs->getEntityManager($purchase);
        $purchase->getPlace()->updateLastVisit();
        $eventArgs->getEntityManager()->flush($purchase->getPlace());
        $this->mailManager->sendPurchaseInvoiceToCustomer($purchase);
    }

    public function postRemove(Purchase $purchase, LifecycleEventArgs $eventArgs)
    {
        $purchase->getPlace()->updateLastVisit();
        $eventArgs->getEntityManager()->flush($purchase->getPlace());
    }
}