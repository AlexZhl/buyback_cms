<?php

namespace Corporation\AdminAreaBundle\Controller;

use Corporation\AdminAreaBundle\Entity\Purchase;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

class StatsAdminController extends CRUDController
{
    public function listAction(Request $request = null)
    {
        $filter = $this->getRequest()->get('filter');
        $startDate = isset($filter['startDate']) && $filter['startDate'] ? $filter['startDate'] : null;
        $endDate = isset($filter['endDate']) && $filter['endDate'] ? $filter['endDate'] : null;

        $places = $this->get('corporation.adminarea.manager.place')->findAll();
        $users = $this->get('sonata.user.manager.user')->findAll();

        $qb = $this->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $qb
            ->select('re.name, SUM(pu.total) as total, SUM(pu.totalScreens) as totalScreens')
            ->from(Purchase::class, 'pu')
            ->join('pu.place', 'pl')
            ->join('pl.region', 're')
            ->groupBy('re')
            ->orderBy('total', 'DESC')
        ;
        if ($startDate) {
            $qb->andWhere('pu.createdAt > :startDate');
            $qb->setParameter('startDate', $startDate);
        }
        if ($endDate) {
            $qb->andWhere('pu.createdAt < :endDate');
            $qb->setParameter('endDate', $endDate);
        }
        $regions = $qb->getQuery()->getResult();

        $qb = $this->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $qb
            ->select('CONCAT(pl.name, \', \', pl.address) as nameAddress, SUM(pu.total) as total, SUM(pu.totalScreens) as totalScreens')
            ->from(Purchase::class, 'pu')
            ->join('pu.place', 'pl')
            ->groupBy('pl')
            ->orderBy('total', 'DESC')
            ->setMaxResults(16)
        ;
        if ($startDate) {
            $qb->andWhere('pu.createdAt > :startDate');
            $qb->setParameter('startDate', $startDate);
        }
        if ($endDate) {
            $qb->andWhere('pu.createdAt < :endDate');
            $qb->setParameter('endDate', $endDate);
        }
        $places = $qb->getQuery()->getResult();

        $qb = $this->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $qb
            ->select('CONCAT(u.firstname, \' \', u.lastname) as fullname, sum(p.total) as total, sum(p.totalScreens) as totalScreens')
            ->from(Purchase::class, 'p')
            ->join('p.purchaser', 'u')
            ->groupBy('p.purchaser')
            ->orderBy('total', 'DESC')
        ;
        if ($startDate) {
            $qb->andWhere('p.createdAt > :startDate');
            $qb->setParameter('startDate', $startDate);
        }
        if ($endDate) {
            $qb->andWhere('p.createdAt < :endDate');
            $qb->setParameter('endDate', $endDate);
        }
        $users = $qb->getQuery()->getResult();

        return $this->render('@CorporationAdminArea/Stats/list.html.twig', [
            'action' => 'list',
            'places' => $places,
            'regions' => $regions,
            'users' => $users,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }
}
