<?php

namespace Corporation\AdminAreaBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\Request;

class PlaceAdminController extends CRUDController
{
    /**
     * Edit action.
     *
     * @param int|string|null $id
     *
     * @return Response|RedirectResponse
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     */
    public function editAction($id = null)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('ROLE_ADMIN')) {
            $request = $this->getRequest();

            $id = $request->get($this->admin->getIdParameter());
            $existingObject = $this->admin->getObject($id);

            if (!$existingObject) {
                throw $this->createNotFoundException(sprintf('unable to find the object with id: %s', $id));
            }

            $place = $this->get('corporation.adminarea.manager.place')->findOneBy([
                $this->admin->getIdParameter() => $id,
                'region' => $this->get('security.token_storage')->getToken()->getUser()->getRegions()->toArray(),
            ]);

            if (!$place) {
                throw new AccessDeniedException('Access Denied');
            }
        }

        return parent::editAction($id);
    }

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionAssignToMe(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $this->admin->checkAccess('edit');

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $modelManager = $this->admin->getModelManager();
        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {
                $selectedModel->setManager($user);
                $modelManager->update($selectedModel);
            }
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', 'flash_batch_assign_to_me_error');

            return new RedirectResponse(
                $this->admin->generateUrl('list', [
                    'filter' => $this->admin->getFilterParameters()
                ])
            );
        }

        $this->addFlash('sonata_flash_success', 'flash_batch_assign_to_me_success');

        return new RedirectResponse(
            $this->admin->generateUrl('list', [
                'filter' => $this->admin->getFilterParameters()
            ])
        );
    }
}
