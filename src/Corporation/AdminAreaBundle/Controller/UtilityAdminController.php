<?php

namespace Corporation\AdminAreaBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class UtilityAdminController extends CRUDController
{
    public function listAction(Request $request = null)
    {
        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: 0');

        $dir = $this->getParameter('kernel.logs_dir');
        $dirFiles = scandir($dir);
        $logs = [];
        foreach ($dirFiles as $k => $log) {
            if ($log != '.gitkeep') {
                $logs[$k] = [
                    'dir' => $dir,
                    'name' => $log,
                    'lastEditedAt' => date(filemtime($dir . DIRECTORY_SEPARATOR . $log)),
                ];
            }
        }

        if (!empty($logs[0]) && ('.' == $logs[0]['name'])) {
            unset($logs[0]);
        }

        if (!empty($logs[1]) && ('..' == $logs[1]['name'])) {
            unset($logs[1]);
        }

        usort($logs, function ($a, $b) {
            return (filemtime($a['dir'] . DIRECTORY_SEPARATOR . $a['name'])
                < filemtime($b['dir'] . DIRECTORY_SEPARATOR . $b['name']));
        });

        return $this->render('@CorporationAdminArea/Utility/log_files_list.html.twig', [
            'logs' => $logs
        ]);
    }

    public function clearCacheAction(Request $request = null)
    {
        ini_set('max_execution_time', 300);
        
        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: 0');

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $inputs = [
//            new StringInput('sonata:cache:flush-all'),
            new StringInput('doctrine:cache:clear-metadata'),
            new StringInput('doctrine:cache:clear-query'),
            new StringInput('doctrine:cache:clear-result'),
            new StringInput('cache:clear'),
        ];

        $content = '';
        foreach ($inputs as $input) {
            $output = new BufferedOutput();
            $application->run($input, $output);

            $outputString = $output->fetch();
            if (strpos(strtolower($outputString), 'exception') === false) {
                $this->addFlash('sonata_flash_success', $outputString);
            } else {
                $this->addFlash('sonata_flash_info', $outputString);
            }
        }

        if (function_exists('opcache_reset')) {
            opcache_reset();
        }

        return $this->redirectToRoute('utility_list');
    }

    public function getLogAction(Request $request = null)
    {
        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: 0');

        $dir = $this->getParameter('kernel.logs_dir');

        if (!$fileName = $request->get('filename')) {
            throw $this->createNotFoundException();
        }

        $filePath = $dir . '/' . $fileName;

        $fs = new Filesystem();
        if (!$fs->exists($filePath)) {
            throw $this->createNotFoundException();
        }

        $response = new BinaryFileResponse($filePath);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
        );

        return $response;
    }
}
