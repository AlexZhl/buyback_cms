<?php

namespace Corporation\AdminAreaBundle\Manager;

use Corporation\AdminAreaBundle\Entity\Purchase;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailManager
{
    protected $mailer;
    protected $noreplyEmail;
    protected $noreplyName;
    protected $container;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container, $noreplyEmail, $noreplyName)
    {
        $this->mailer = $mailer;
        $this->container = $container;
        $this->noreplyEmail = $noreplyEmail;
        $this->noreplyName = $noreplyName;
    }

    public function sendMail($params)
    {
        if (!isset($params['content_type'])) {
            $params['content_type'] = 'text/html';
        }

        if (!isset($params['from'])) {
            $params['from'] = [ $this->noreplyEmail => $this->noreplyName ];
        }

        $message = \Swift_Message::newInstance()
            ->setSubject($params['subject'])
            ->setFrom($params['from'])
            ->setTo($params['email'])
            ->setBody($params['body'])
            ->setContentType($params['content_type']);

//        if (!empty($params['attachments'])) {
//            $fs = new Filesystem();
//            foreach ($params['attachments'] as $attachment) {
//                if ( ! $fs->exists($attachment)) {
//                    continue;
//                }
//
//                try {
//                    $message->attach(\Swift_Attachment::fromPath($attachment));
//                } catch (\Exception $e) {
//                    $this->logger->critical('[mail.consumer] Attachments error.', ['error' => $e->getMessage()]);
//                    return;
//                }
//            }
//        }

        $this->mailer->send($message);
        $this->mailer->getTransport()->stop();
    }

    public function sendPurchaseInvoiceToCustomer(Purchase $purchase)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('New Invoice from M2Bwholesale')
            ->setFrom([ $this->noreplyEmail => $this->noreplyName ])
            ->setTo($purchase->getCustomerEmail())
            ->setContentType('text/html')
        ;

        $data = $purchase->getSignatureImage();
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $img_data = base64_decode($data);

        $cid = $message->embed(new \Swift_Image($img_data, 'image.jpg', 'image/jpeg'));

        $message->setBody($this->container->get('templating')->render('@CorporationAdminArea/Email/invoice_template.html.twig', [
            'purchase' => $purchase,
            'cid' => $cid,
        ]));

        $this->mailer->send($message);
        $this->mailer->getTransport()->stop();
    }
}
