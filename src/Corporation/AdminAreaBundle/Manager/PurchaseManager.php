<?php

namespace Corporation\AdminAreaBundle\Manager;

use Corporation\AdminAreaBundle\Entity\Purchase;
use Corporation\UserAreaBundle\Manager\BaseManager;

class PurchaseManager extends BaseManager
{
    public function findTopEmployees($maxResults, $month = false)
    {
        $employeesQB = $this->getEm()->createQueryBuilder();

        $employeesQB
            ->select('CONCAT(u.firstname, \' \', u.lastname) as fullname, sum(p.total) as total, sum(p.totalScreens) as totalScreens, p.createdAt')
            ->from(Purchase::class, 'p')
            ->join('p.purchaser', 'u')
            ->groupBy('p.purchaser')
            ->orderBy('total', 'DESC')
            ->setMaxResults($maxResults)
            ->andWhere('p.voided = false')
            ->andWhere('p.createdAt > :date_from')
        ;
        if ($month) {
            $employeesQB
                ->setParameter('date_from',  new \DateTime('first day of this month'))
                ->andWhere('p.createdAt < :date_to')
                ->setParameter('date_to',  new \DateTime('last day of this month'))
            ;
        } else {
            $employeesQB->setParameter('date_from', new \DateTime('today'));
        }

        return $employeesQB->getQuery()->getResult();
    }
}
