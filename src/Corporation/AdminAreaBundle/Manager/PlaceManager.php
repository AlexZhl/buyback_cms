<?php

namespace Corporation\AdminAreaBundle\Manager;

use Application\Sonata\UserBundle\Entity\User;
use Corporation\UserAreaBundle\Manager\BaseManager;

class PlaceManager extends BaseManager
{
    public function findPlacesToVisit(User $user = null)
    {
        $query = $this->repository->createQueryBuilder('place')
            ->where('place.lastVisit <= :today')
            ->setParameter(':today', new \DateTime())
        ;

        if ($user) {
            $regions = $user->getRegions();
            $query->where('place.region = :regions');
            $query->setParameter(':regions', $regions);
        }

        return $query->getQuery()->getResult();
    }
}
