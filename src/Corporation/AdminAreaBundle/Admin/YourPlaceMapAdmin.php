<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Place;

class YourPlaceMapAdmin extends PlaceAdmin
{
    protected $baseRouteName = 'your_place_map';
    protected $baseRoutePattern = 'your-place-map';
    protected $translationDomain = 'YourPlaceMapAdmin';

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $user = $this->tokenStorage->getToken()->getUser();

        $query->andWhere($query->getRootAliases()[0] . '.manager = :thisUser');
        $query->setParameter('thisUser', $user);
        $query->andWhere($query->getRootAliases()[0] . '.type = :placeType');
        $query->setParameter('placeType', Place::PLACE_TYPE_SELLING);

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $query->andWhere($query->expr()->in($query->getRootAliases()[0] . '.region', ':userRegions'));
            $query->setParameter('userRegions', $user->getRegions()->toArray());
        }

        return $query;
    }
}
