<?php

namespace Corporation\AdminAreaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class StatsAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'stats';
    protected $baseRoutePattern = 'stats';
    protected $translationDomain = 'StatsAdmin';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept('list');

        $collection->add('clearcache', 'clear-cache', [
            '_controller' => 'Corporation\AdminAreaBundle\Controller\UtilityAdminController::clearCacheAction',
            'namespace' => 'default',
        ]);

        $collection->add('getlog', 'log', [
            '_controller' => 'Corporation\AdminAreaBundle\Controller\UtilityAdminController::getLogAction',
            'namespace' => 'default',
        ]);
    }
}
