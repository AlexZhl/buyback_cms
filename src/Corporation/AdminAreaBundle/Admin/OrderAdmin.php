<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Order;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class OrderAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'order';
    protected $baseRoutePattern = 'order';
    protected $translationDomain = 'OrderAdmin';
    protected $parentAssociationMapping = 'customer';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Main', ['class' => 'col-md-6'])
                ->add('customerEmail')
                ->add('status', 'corporation_adminarea_order_status', ['translation_domain' => 'CorporationAdminAreaBundle'])
                ->add('createdAt', 'sonata_type_datetime_picker', array('disabled' => true, 'required' => false))
            ->end()
            ->with('Payment Details', ['class' => 'col-md-6'])
                ->add('paymentMethod', 'corporation_adminarea_order_payment_method', ['translation_domain' => 'CorporationAdminAreaBundle'])
                ->add('transactionNumber')
                ->add('paymentStatus', 'corporation_adminarea_order_payment_status', ['translation_domain' => 'CorporationAdminAreaBundle'])
            ->end()
            ->with('Shipping Details', ['class' => 'col-md-12'])
                ->add('deliveryMethod', 'corporation_adminarea_order_delivery_method', ['translation_domain' => 'CorporationAdminAreaBundle'])
                ->add('deliveryStatus', 'corporation_adminarea_order_delivery_status', ['translation_domain' => 'CorporationAdminAreaBundle'])
                ->add('shippingFirstname')
                ->add('shippingLastname')
                ->add('shippingAddressLine1')
                ->add('shippingAddressLine2')
                ->add('shippingCity')
                ->add('shippingRegion')
                ->add('shippingPostalCode')
                ->add('shippingCountry')
                ->add('phone')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('status')
            ->add('deliveryStatus')
            ->add('createdAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('status')
            ->addIdentifier('deliveryStatus')
            ->addIdentifier('createdAt')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Order
            ? $object->getTransactionNumber()
            : 'Order';
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $menu->addChild(
            'Order',
            ['uri' => $admin->generateUrl('edit', ['id' => $id])]
        );

        $menu->addChild(
            'Order Elements',
            [ 'uri' => $this->getChild('corporation.adminarea.admin.order_element')->generateUrl('list', ['id' => $id]) ]
        );
    }
}
