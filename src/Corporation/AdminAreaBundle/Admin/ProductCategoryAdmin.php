<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\ProductCategory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductCategoryAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'product_category';
    protected $baseRoutePattern = 'product_category';
    protected $translationDomain = 'ProductCategoryAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('icon', 'sonata_type_model_list', [ 'required' => false, ], [
                'link_parameters' => [
                    'context' => 'product_category',
                    'filter' => ['context' => ['value' => 'product_category']],
                    'provider' => '',
                ]
            ])
            ->add('products', 'sonata_type_model', [
                'class' => 'Corporation\AdminAreaBundle\Entity\Product',
                'property' => 'title',
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }

    public function toString($object)
    {
        return $object instanceof ProductCategory
            ? $object->getName()
            : 'Product Category'; // shown in the breadcrumb on the create view
    }
}
