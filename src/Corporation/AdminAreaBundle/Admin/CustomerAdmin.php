<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Customer;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class CustomerAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'customer';
    protected $baseRoutePattern = 'customer';
    protected $translationDomain = 'CustomerAdmin';
    protected $formOptions = [
        'validation_groups' => 'CustomerAdmin',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-12'))
                ->add('email')
                ->add('isActive', null, array('required' => false))
                ->add('trusted', null, array('required' => false))
                ->add('createdAt', 'sonata_type_datetime_picker', array('disabled' => true, 'required' => false))
            ->end()
            ->with('Shipment Details', array('class' => 'col-md-12'))
                ->add('fullName', null, array('required' => false))
                ->add('addressLine1', null, array('required' => false, 'label' => 'Address'))
                ->add('addressLine2', null, array('required' => false, 'label' => 'Address Line 2'))
                ->add('postalCode', null, array('required' => false))
                ->add('city', null, array('required' => false))
                ->add('region', null, array('required' => false))
                ->add('country', null, array('required' => false))
                ->add('phone', null, array('required' => false))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('fullName')
            ->add('email')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('fullName')
            ->addIdentifier('email')
            ->addIdentifier('createdAt')
        ;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $menu->addChild(
            'Customer',
            ['uri' => $admin->generateUrl('edit', ['id' => $id])]
        );

//        $menu->addChild(
//            'Orders',
//            [ 'uri' => $this->getChild('corporation.adminarea.admin.order')->generateUrl('list', ['id' => $id]) ]
//        );
        $menu->addChild(
            'Shippings',
            [ 'uri' => $this->getChild('corporation.adminarea.admin.shipping')->generateUrl('list', ['id' => $id]) ]
        );
    }

    public function toString($object)
    {
        return $object instanceof Customer
            ? $object->getEmail()
            : 'Customer';
    }
}
