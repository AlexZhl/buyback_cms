<?php

namespace Corporation\AdminAreaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PurchaseElementAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'purchase_element';
    protected $baseRoutePattern = 'purchase-element';
    protected $translationDomain = 'PurchaseElementAdmin';
    protected $parentAssociationMapping = 'purchase';
    protected $tokenStorage;
    protected $authorizationChecker;

    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if (!$this->isChild() && !$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $user = $this->tokenStorage->getToken()->getUser();
            $query
                ->join($query->getRootAlias() . '.purchase', 'purchase')
                ->andWhere(
                    $query->expr()->in('purchase.purchaser', ':user')
                )
            ;
            $query->setParameter('user', $user);
        }

        return $query;
    }


    public function toString($object)
    {
        return 'Purchase ELement';
    }
}
