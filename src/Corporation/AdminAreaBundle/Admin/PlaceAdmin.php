<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Place;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Corporation\AdminAreaBundle\Form\Type\PlaceTypeType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class PlaceAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'place';
    protected $baseRoutePattern = 'place';
    protected $translationDomain = 'PlaceAdmin';
    protected $datagridValues = [
        '_sort_by' => 'lastVisit',
    ];
    protected $tokenStorage;
    protected $authorizationChecker;

    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $userRegionsArray = $this->tokenStorage->getToken()->getUser()->getRegions()->toArray();
            $query->andWhere(
                $query->expr()->in($query->getRootAliases()[0] . '.region', ':userRegions')
            );
            $query->setParameter('userRegions', $userRegionsArray);
        }

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId()) {
            if (!$this->authorizationChecker->isGranted('ROLE_STAFF')) {
                $formMapper
                    ->with('Place', ['class' => 'col-md-12'])
                        ->add('type', PlaceTypeType::class, ['translation_domain' => 'CorporationAdminAreaBundle'])
                    ->end();
            } else {
                $formMapper
                    ->tab('General')
                        ->with('Main', ['class' => 'col-md-6'])
                            ->add('type', PlaceTypeType::class, ['translation_domain' => 'CorporationAdminAreaBundle'])
                            ->add('notes')
                            ->add('nextVisit', 'sonata_type_date_picker', ['disabled' => false, 'required' => false])
                            ->add('newScreensEachDay')
                        ->end()
                        ->with('Info', ['class' => 'col-md-6'])
                            ->add('lastVisit', 'sonata_type_date_picker', ['disabled' => true, 'required' => false])
                            ->add('managerNameDyn', 'text', [
                                'disabled' => true,
                                'required' => false,
                                'label' => 'Manager Name',
                            ])
                            ->add('lastPurchaseTotal', 'text', ['disabled' => true, 'required' => false])
                            ->add('lastPurchaseScreens', 'text', ['disabled' => true, 'required' => false])
                            ->add('lastPurchaserNameDyn', 'text', [
                                'disabled' => true,
                                'required' => false,
                                'label' => 'Last Purchase By',
                            ])
                        ->end()
                        ->with('Settings', ['class' => 'col-md-12'])
                            ->add('latlng', 'oh_google_maps', [
                                'default_lat' => 25.875928,
                                'default_lng' => -80.209289,
                            ])
                            ->add('newScreensEachDay', 'number')
                            ->add('enabled')
                        ->end()
                    ->end()
                    ->tab('Details')
                        ->with('Main', ['class' => 'col-md-6'])
                            ->add('name', 'text')
                            ->add('phone', null, ['required' => true])
                            ->add('type', PlaceTypeType::class, ['translation_domain' => 'CorporationAdminAreaBundle'])
                        ->end()
                        ->with('Info', ['class' => 'col-md-6'])
                            ->add('address', 'text', ['disabled' => true, 'required' => false])
                            ->add('region', 'sonata_type_model', [
                                'class' => 'Corporation\AdminAreaBundle\Entity\Region',
                                'property' => 'name',
                                'required' => false,
                                'disabled' => true,
                            ])
                        ->end()
                    ->end()
                ;
            }
        } else {
            $formMapper
                ->with('Place', ['class' => 'col-md-12'])
                    ->add('name', 'text')
                    ->add('phone', null, ['required' => true])
                    ->add('type', PlaceTypeType::class, ['translation_domain' => 'CorporationAdminAreaBundle'])
                    ->add('newScreensEachDay', 'number')
                    ->add('nextVisit', 'sonata_type_date_picker', ['disabled' => false, 'required' => false])
                    ->add('notes')
                    ->add('enabled')
                ->end()
                ->with('Location', ['class' => 'col-md-12'])
                    ->add('latlng', 'oh_google_maps', [
                        'default_lat' => 25.875928,
                        'default_lng' => -80.209289,
                    ])
                ->end()
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('address')
            ->add('phone')
            ->add('type', null, [], PlaceTypeType::class, ['translation_domain' => 'CorporationAdminAreaBundle'])
            ->add('lat')
            ->add('lng')
            ->add('notes')
            ->add('manager', null, [], ModelType::class, [
                'model_manager' => $this->getConfigurationPool()->getAdminByAdminCode('sonata.user.admin.user')->getModelManager(),
                'choices' => [ $this->tokenStorage->getToken()->getUser() ],
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('address')
            ->add('phone')
            ->add('type', PlaceTypeType::class, ['template' => 'CorporationAdminAreaBundle:CRUD:place_list_type.html.twig',])
            ->add('region.name')
            ->add('lastVisit')
            ->add('nextVisit')
            ->add('lastPurchaseTotal')
            ->add('lastPurchaseScreens')
            ->add('lastPurchaser.fullName')
            ->add('expectedScreens')
            ->add('manager.fullName')
        ;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $menu->addChild(
            'Place',
            ['uri' => $admin->generateUrl('edit', ['id' => $id])]
        );

        $menu->addChild(
            'Purchases',
            [ 'uri' => $this->getChild('corporation.adminarea.admin.purchase')->generateUrl('list', ['id' => $id]) ]
        );
    }

    public function toString($object)
    {
        return $object instanceof Place ? $object->getName() : 'Place';
    }

    protected function configureBatchActions($actions)
    {
        unset($actions['delete']);
        if ($this->hasRoute('edit') && $this->hasAccess('edit')) {
            $actions['assign_to_me'] = [
                'ask_confirmation' => true
            ];
        }
        return $actions;
    }

    public function getExportFields()
    {
        return [
            'name', 'address', 'phone', 'lat', 'lng', 'newScreensEachDay', 'lastVisit',
            'type', 'notes', 'enabled',
            'lastPurchaseTotal', 'lastPurchaseScreens', 'region.name',
        ];
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // Override any method you want to add in "PlaceAdminController"
        // to restrict access by "Regions" assigned to current "User". (see PlaceAdminController::editAction)
//        $collection->clearExcept(['list', 'create', 'edit', 'delete', 'export', 'batch']);
    }
}
