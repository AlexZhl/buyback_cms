<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Place;
use Corporation\AdminAreaBundle\Entity\Purchase;
use Corporation\AdminAreaBundle\Entity\PurchaseElement;
use Corporation\AdminAreaBundle\Form\Type\ScreenModelsQuantityType;
use Corporation\AdminAreaBundle\Form\Type\SignatureType;
use Corporation\AdminAreaBundle\Manager\PlaceManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Component\Form\CallbackTransformer;

class PurchaseAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'purchase';
    protected $baseRoutePattern = 'purchase';
    protected $translationDomain = 'PurchaseAdmin';
    protected $parentAssociationMapping = 'place';
    protected $tokenStorage;
    protected $authorizationChecker;
    protected $placeManger;
    protected $sessionService;

    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function setPlaceManager(PlaceManager $placeManager)
    {
        $this->placeManger = $placeManager;
    }

    public function setSessionService(Session $session)
    {
        $this->sessionService = $session;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $userRegionsArray = $this->tokenStorage->getToken()->getUser()->getRegions()->toArray();
            $query->andWhere(
                $query->expr()->in('s_place.region', ':userRegions')
            );
            $query->setParameter('userRegions', $userRegionsArray);
        }

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId()) {
            if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                if ($subject->getPurchaser() != $this->tokenStorage->getToken()->getUser()) {
                    throw new AccessDeniedException('Access Denied');
                }
            }
            if ($subject->getVoided()) {
                $formMapper
                    ->add('voided', null, ['disabled' => true])
                ;
                return;
            }

            $formMapper->add('voided', null, ['label' => 'Void']);
            return;
        }

        $voidedInvoice = $this->getModelManager()->findOneBy(Purchase::class, ['voided' => true, 'purchaser' => $this->tokenStorage->getToken()->getUser()]);
        if ($voidedInvoice) {
            $this
                ->sessionService
                ->getFlashBag()
                ->add('sonata_flash_info', sprintf(
                    'You have voided invoices. <a href="%s">Click here</a> to see them',
                    $this->generateUrl('list', ['filter' => ['voided' => ['value' => 1]]])
                ))
            ;
        }

        $formMapper
            ->with('Purchase', ['class' => 'col-md-12'])
                ->add('screenModels', ScreenModelsQuantityType::class, ['required' => false])
            ->end()
            ->with('Details', ['class' => 'col-md-12'])
        ;

        if (!$formMapper->getAdmin()->isChild()) {
            if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                $formMapper->add('place', 'sonata_type_model_autocomplete', [
                    'class' => 'Corporation\AdminAreaBundle\Entity\Place',
                    'property' => ['address', 'name'],
                    'callback' => function ($admin, $properties, $value) {
                        $datagrid = $admin->getDatagrid();
                        $queryBuilder = $datagrid->getQuery();
                        $value = '%' . $value . '%';
                        $queryBuilder
                            ->andWhere('(o.address LIKE :valueAddr OR o.name LIKE :valueName)')
                            ->setParameter('valueAddr', $value)
                            ->setParameter('valueName', $value)
                        ;
                    },
                    'to_string_callback' => function($entity, $property) {
                        return $entity->getName() . ', ' . $entity->getAddress();
                    },
                ], ['admin_code' => 'corporation.adminarea.admin.place']);
            } else {
                $formMapper->add('place', 'sonata_type_model_autocomplete', [
                    'class' => 'Corporation\AdminAreaBundle\Entity\Place',
                    'property' => ['address', 'name'],
                    'to_string_callback' => function($entity, $property) {
                        return $entity->getName() . ', ' . $entity->getAddress();
                    }
                ], ['admin_code' => 'corporation.adminarea.admin.place']);
            }
        }

        $formMapper
            ->add('signatureImage', SignatureType::class, [
                'label' => 'Customer Signature',
            ])
            ->add('customerEmail', EmailType::class, [
                'label' => 'Customer Email (Invoice copy will be mailed)',
            ])
            ->add('createdAt', 'sonata_type_datetime_picker', ['required' => false])
        ;
        if ($this->isGranted('ROLE_MANAGER')) {
            $formMapper->add('purchaser', 'sonata_type_model', [
                'class' => 'Application\Sonata\UserBundle\Entity\User',
                'property' => 'fullName',
                'btn_add' => false,
            ]);
        }
        $formMapper
            ->end()
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('place.name')
            ->add('place.address')
            ->add('createdAt', 'doctrine_orm_datetime_range', array(
                'field_type'=>'sonata_type_datetime_range_picker',
                'field_options' => [
                    'field_options' => [
                        'format' => 'yyyy-MM-dd H:m'
                    ]
                ]
            ))
            ->add('voided')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('purchaser.name')
            ->addIdentifier('totalWithBad')
            ->addIdentifier('totalScreensWithBad')
            ->addIdentifier('place.name')
            ->addIdentifier('place.address')
            ->addIdentifier('createdAt')
            ->add('voided')
        ;
    }

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Purchase', ['class' => 'col-md-12'])
                ->add('purchaseElements')
                ->add( 'totalWithBad', null, ['label' => 'Total Spent With Bad ($)'])
                ->add( 'totalScreensWithBad')
            ->end()
            ->with('Purchase Details', ['class' => 'col-md-12'])
                ->add('place.name')
                ->add('place.address')
                ->add('place.phone')
                ->add('purchaser.name')
                ->add('signatureImageAsHtmlImg', null, [
                    'label' => 'Customer Signature',
                    'safe' => true,
                ])
        ;
        if ($this->getSubject()->getVoided()){
            $showMapper->add('voided');
        }
        $showMapper
            ->end()
        ;
    }

    public function toString($object)
    {
        return 'Purchase';
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('view');
    }
}
