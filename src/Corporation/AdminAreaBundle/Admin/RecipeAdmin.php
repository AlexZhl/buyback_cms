<?php

namespace Corporation\AdminAreaBundle\Admin;

use Application\Sonata\MediaBundle\Document\Media;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;
use Corporation\AdminAreaBundle\Entity\Recipe;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Admin\ORM\MediaAdmin;
use Application\Sonata\MediaBundle\Form\Type\MediaArrayValueType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RecipeAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'recipe';
    protected $baseRoutePattern = 'recipe';
    protected $translationDomain = 'RecipeAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text')
            ->add('slug')
            ->add('recipeSteps', KeyValueType::class, array(
                'key_type' => TextType::class,
                'value_type' => TextType::class,
            ))
            ->add('video', 'sonata_type_model_list', [ 'required' => false, ], [
                'link_parameters' => [
                    'context' => 'recipe',
                    'filter' => ['context' => ['value' => 'recipe']],
                    'provider' => '',
                ]
            ])
            ->add('products', 'sonata_type_model', [
                'label' => 'Related Products',
                'class' => 'Corporation\AdminAreaBundle\Entity\Product',
                'property' => 'title',
                'multiple' => true,
                'required' => false,
            ])
            ->add('enabled')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('enabled')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->addIdentifier('enabled')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Recipe
            ? $object->getTitle()
            : 'Recipe';
    }
}
