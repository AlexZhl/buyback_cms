<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Region;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class RegionAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'region';
    protected $baseRoutePattern = 'region';
    protected $translationDomain = 'RegionAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Region', ['class' => 'col-md-6'])
                ->add('name', 'text')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Region ? $object->getName() : 'Region';
    }
}
