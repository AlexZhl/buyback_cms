<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Product;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ProductAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'product';
    protected $baseRoutePattern = 'product';
    protected $translationDomain = 'ProductAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Main', ['class' => 'col-md-6'])
            ->add('title')
            ->add('slug')
            ->add('description')
            ->add('popular')
            ->add('enabled')
            ->end()
            ->with('Properties', ['class' => 'col-md-6'])
            ->add('price')
            ->add('stock')
            ->add('ounces')
            ->end()
            ->with('Media')
            ->add('mainPhoto', 'sonata_type_model_list', [ 'required' => false, ], [
                'link_parameters' => [
                    'context' => 'product',
                    'filter' => ['context' => ['value' => 'product']],
                    'provider' => '',
                ]
            ])
            ->add('photoGallery', 'sonata_type_model_list', [ 'required' => false, ], [
                'link_parameters' => [
                    'context' => 'product',
                    'filter' => ['context' => ['value' => 'product']],
                    'provider' => '',
                ],
            ])
            ->end()
            ->with('Categories')
            ->add('categories', 'sonata_type_model', [
                'class' => 'Corporation\AdminAreaBundle\Entity\ProductCategory',
                'property' => 'name',
                'multiple' => true,
                'required' => false,
            ])
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('price')
            ->add('stock')
            ->add('enabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->addIdentifier('price')
            ->addIdentifier('stock')
            ->addIdentifier('enabled')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Product
            ? $object->getTitle()
            : 'Product';
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
//        $product = $this->getObject($id);
        $menu->addChild(
            'Product',
            ['uri' => $admin->generateUrl('edit', ['id' => $id])]
        );
        $menu->addChild(
            'Categories',
            [ 'uri' => $this->getChild('corporation.adminarea.admin.product_category')->generateUrl('list', ['id' => $id]) ]
        );
    }
}
