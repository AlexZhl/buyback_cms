<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\OrderElement;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OrderElementAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'order_element';
    protected $baseRoutePattern = 'order_element';
    protected $translationDomain = 'OrderElementAdmin';
    protected $parentAssociationMapping = 'order';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Main')
                ->add('quantity')
                ->add('price')
                ->add('product', 'sonata_type_model', [
                    'class' => 'Corporation\AdminAreaBundle\Entity\Product',
                    'property' => 'title',
                    'required' => false,
                ])
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('quantity')
            ->add('product.title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('quantity')
            ->addIdentifier('product.title')
        ;
    }

    public function toString($object)
    {
        return $object instanceof OrderElement
            ? $object->getProduct()->getTitle()
            : 'Order Element';
    }
}
