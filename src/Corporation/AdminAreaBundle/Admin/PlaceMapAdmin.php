<?php

namespace Corporation\AdminAreaBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

class PlaceMapAdmin extends PlaceAdmin
{
    protected $baseRouteName = 'place_map';
    protected $baseRoutePattern = 'place-map';
    protected $translationDomain = 'PlaceMapAdmin';
    protected $maxPerPage = 'All';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}
