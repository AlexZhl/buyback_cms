<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Order;
use Corporation\AdminAreaBundle\Entity\Shipping;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ShippingAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'shipping';
    protected $baseRoutePattern = 'shipping';
    protected $translationDomain = 'ShippingAdmin';
    protected $parentAssociationMapping = 'customer';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        if ($subject->getId()) {
            $formMapper
                ->with('Main', ['class' => 'col-md-6'])
                    ->add('approved')
                    ->add('identificationNumber', 'text', ['disabled' => true, 'required' => false])
                    ->add('email', 'text', ['disabled' => true, 'required' => false])
                    ->add('phone', 'text', ['disabled' => true, 'required' => false])
                    ->add('shippingLabelsQuantity', 'text', ['disabled' => true, 'required' => false])
                    ->add('charges', 'text', ['disabled' => true, 'required' => false])
                    ->add('createdAt', 'sonata_type_datetime_picker', array('disabled' => true, 'required' => false))
                ->end()
                ->with('Shipping Details', ['class' => 'col-md-12'])
                    ->add('fullName', 'text', ['disabled' => true, 'required' => false])
                    ->add('addressLine1', 'text', ['disabled' => true, 'required' => false])
                    ->add('addressLine2', 'text', ['disabled' => true, 'required' => false])
                    ->add('city', 'text', ['disabled' => true, 'required' => false])
                    ->add('region', 'text', ['disabled' => true, 'required' => false])
                    ->add('postalCode', 'text', ['disabled' => true, 'required' => false])
                    ->add('country', 'text', ['disabled' => true, 'required' => false])
                ->end()
            ;
        } else {
            $formMapper
                ->with('Main', ['class' => 'col-md-6'])
                    ->add('customer', 'sonata_type_model', [
                        'class' => 'Corporation\AdminAreaBundle\Entity\Customer',
                        'property' => 'email',
                        'required' => false,
                    ])
                    ->add('phone', 'text')
                    ->add('shippingLabelsQuantity', 'text', ['disabled' => false, 'required' => true])
                ->end()
                ->with('Shipping Details', ['class' => 'col-md-12'])
                    ->add('fullName', 'text', ['required' => true,])
                    ->add('addressLine1', 'text', ['required' => true,])
                    ->add('addressLine2', 'text', ['required' => true,])
                    ->add('city', 'text', ['required' => true,])
                    ->add('region', 'text', ['required' => true,])
                    ->add('postalCode', 'text', ['required' => true,])
                    ->add('country', 'text', ['required' => true,])
                ->end()
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email')
            ->add('phone')
            ->add('fullName')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('identificationNumber')
            ->addIdentifier('fullName')
            ->addIdentifier('phone')
            ->addIdentifier('email')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Shipping
            ? $object->getIdentificationNumber()
            : 'Shipping';
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $menu->addChild(
            'Shipping',
            ['uri' => $admin->generateUrl('edit', ['id' => $id])]
        );
        $menu->addChild(
            'Shipping Labels',
            [ 'uri' => $this->getChild('corporation.adminarea.admin.shipping_label')->generateUrl('list', ['id' => $id]) ]
        );
    }
}
