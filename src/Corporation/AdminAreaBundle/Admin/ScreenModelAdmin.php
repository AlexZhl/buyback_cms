<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\ScreenModel;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ScreenModelAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'screen_model';
    protected $baseRoutePattern = 'screen_model';
    protected $translationDomain = 'ScreenModelAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Screen Model', ['class' => 'col-md-6'])
                ->add('name')
                ->add('defaultOemPrice')
                ->add('defaultIssuesPrice')
                ->add('defaultBadPrice')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('defaultOemPrice')
            ->add('defaultIssuesPrice')
            ->add('defaultBadPrice')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('defaultOemPrice')
            ->addIdentifier('defaultIssuesPrice')
            ->addIdentifier('defaultBadPrice')
        ;
    }

    public function toString($object)
    {
        return $object instanceof ScreenModel ? $object->getName() : 'ScreenModel';
    }
}
