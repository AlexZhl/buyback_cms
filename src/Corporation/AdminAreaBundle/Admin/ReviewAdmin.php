<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Review;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ReviewAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'review';
    protected $baseRoutePattern = 'review';
    protected $translationDomain = 'ReviewAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Author', ['class' => 'col-md-6'])
            ->add('author', 'text', ['required' => false])
            ->add('socialLink', 'text', ['required' => false])
            ->add('photo', 'sonata_type_model_list', [ 'required' => false, ], [
                'link_parameters' => [
                    'context' => 'review',
                    'filter' => ['context' => ['value' => 'review']],
                    'provider' => '',
                ]
            ])
            ->end()
            ->with('Review', ['class' => 'col-md-6'])
            ->add('rating', null, ['attr' => ['min' => '1', 'max' => '5']])
            ->add('text')
            ->add('enabled')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('author')
            ->add('socialLink')
            ->add('rating')
            ->add('enabled')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('author')
            ->addIdentifier('socialLink')
            ->addIdentifier('rating')
            ->addIdentifier('enabled')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Review
            ? $object->getAuthor()
            : 'Review';
    }
}
