<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\ShippingLabel;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class ShippingLabelAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'shipping_label';
    protected $baseRoutePattern = 'shipping-label';
    protected $translationDomain = 'ShippingLabelAdmin';
    protected $parentAssociationMapping = 'shipping';

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->with('Main')
                ->add('trackingNumber')
                ->add('weight')
                ->add('height')
                ->add('width')
                ->add('length')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('trackingNumber')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('trackingNumber')
        ;
    }

    public function toString($object)
    {
        return $object instanceof ShippingLabel ? $object->getTrackingNumber() : 'Shipping Label';
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'show']);
    }
}
