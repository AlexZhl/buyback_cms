<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Discount;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DiscountAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'discount';
    protected $baseRoutePattern = 'discount';
    protected $translationDomain = 'DiscountAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('type')
            ->add('amount')
            ->add('amount_type')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('type')
            ->add('amount')
            ->add('amount_type')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('type')
            ->add('amount')
            ->add('amount_type')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Discount
            ? $object->getTitle()
            : 'Discount';
    }
}
