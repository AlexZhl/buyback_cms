<?php

namespace Corporation\AdminAreaBundle\Admin;

use Corporation\AdminAreaBundle\Entity\Question;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class QuestionAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'question';
    protected $baseRoutePattern = 'question';
    protected $translationDomain = 'QuestionAdmin';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('question', 'textarea')
            ->add('answer', 'textarea')
            ->add('products', 'sonata_type_model', [
                'label' => 'Related Products',
                'class' => 'Corporation\AdminAreaBundle\Entity\Product',
                'property' => 'title',
                'multiple' => true,
                'required' => false,
            ])
            ->add('email')
            ->add('enabled')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('question')
            ->add('answer')
            ->add('enabled')
        ;
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('question')
            ->addIdentifier('enabled')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Question ? 'Question' : 'Question';
    }
}
