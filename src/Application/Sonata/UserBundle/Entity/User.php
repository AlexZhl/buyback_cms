<?php

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="fos_user_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->regions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getOneRole()
    {
        if (isset($this->roles[0])) {
            return $this->roles[0];
        } else {
            return null;
        }
    }

    public function setOneRole($role)
    {
        $this->roles = [ $role ];

        return $this;
    }

    private $regions;

    /**
     * Add region
     *
     * @param \Corporation\AdminAreaBundle\Entity\Region $region
     * @return User
     */
    public function addRegion(\Corporation\AdminAreaBundle\Entity\Region $region)
    {
        if ($this->regions->contains($region)) {
            return $this;
        }
        $this->regions->add($region);
        $region->addUser($this);

        return $this;
    }

    /**
     * Remove region
     *
     * @param \Corporation\AdminAreaBundle\Entity\Region $region
     * @return User
     */
    public function removeRegion(\Corporation\AdminAreaBundle\Entity\Region $region)
    {
        if (!$this->regions->contains($region)) {
            return $this;
        }
        $this->regions->removeElement($region);
        $region->removeUser($this);

        return $this;
    }

    /**
     * Get regions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

    public function getName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
