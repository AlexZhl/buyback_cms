<?php

namespace Application\Sonata\UserBundle\Admin\Entity;

use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;
use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Form\Type\SecurityRolesType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserAdmin extends BaseUserAdmin
{
    protected $tokenStorage;

    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('createdAt')
        ;

        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add('impersonating', 'string', ['template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig'])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add('username')
            ->add('locked')
            ->add('email')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
                ->add('username')
                ->add('email')
            ->end()
            ->with('Profile')
                ->add('dateOfBirth')
                ->add('firstname')
                ->add('lastname')
                ->add('phone')
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            if ($this->tokenStorage->getToken()->getUser() != $this->getSubject()) {
                throw new AccessDeniedException('Access Denied');
            }
            $formMapper
                ->with('form.area_profile', ['class' => 'col-md-6'])
                    ->add('firstname', null, array('required' => true))
                    ->add('lastname', null, array('required' => true))
                    ->add('phone', null, array('required' => false))
                ->end()
                ->with('form.area_settings', ['class' => 'col-md-6'])
                    ->add('plainPassword', 'text', [
                        'label' => 'New Password',
                        'required' => false,
                    ])
                ->end()
            ;
            return;
        }

        $formMapper
            ->with('form.area_general', ['class' => 'col-md-6'])
                ->add('username')
                ->add('email')
                ->add('plainPassword', 'text', array(
                    'label' => 'New Password',
                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId())),
                ))
            ->end()
            ->with('form.area_profile', ['class' => 'col-md-6'])
                ->add('firstname', null, array('required' => true))
                ->add('lastname', null, array('required' => true))
                ->add('phone', null, array('required' => false))
            ->end()
            ->with('form.area_status', ['class' => 'col-md-6'])
                ->add('locked', null, array('required' => false))
                ->add('expired', null, array('required' => false))
                ->add('enabled', null, array('required' => false))
                ->add('credentialsExpired', null, array('required' => false))
            ->end()
            ->with('form.area_security_details', ['class' => 'col-md-6'])
                ->add('oneRole', ChoiceType::class, [
                    'choices' => [
                        'ROLE_STAFF_BEGINNER' => 'Staff Beginner',
                        'ROLE_STAFF' => 'Staff',
                        'ROLE_MANAGER' => 'Manager',
                        'ROLE_ADMIN' => 'Admin',
                    ],
                    'multiple' => false,
                ])
                ->add('regions', 'sonata_type_model', [
                    'class' => 'Corporation\AdminAreaBundle\Entity\Region',
                    'property' => 'name',
                    'multiple' => true,
                    'required' => false,
                ])
//                ->add('realRoles', SecurityRolesType::class, array(
//                    'label' => 'form.label_roles',
//                    'expanded' => true,
//                    'multiple' => true,
//                    'required' => false,
//                ))
            ->end()
        ;
    }
}
